<?php
define('NEWLINE', "\n");

//SOAP client
$wsdl = 'http://climbservices.cloudapp.net/RemoteClimb_GetSmallGroups.svc?wsdl';

$soapClient = new SoapClient($wsdl, array('cache_wsdl' => 0));

//SOAP call
$parameters = new stdClass();
$parameters->_parentid = 'fb73af9b-e94a-4091-8eaa-73ed9f155481';
$parameters->_type = '(All)';
$parameters->_location = '(All)';
$parameters->_dynamic = '(All)';
$parameters->_daymeeting = '(All)';
$parameters->_childcare = 'N';
$parameters->_childrenwelcome = 'N';

try
{
	$result = $soapClient->GetGroups($parameters);
}
catch(SoapFault $fault)
{
	echo "Fault code: {$fault->faultcode}" . NEWLINE;
	echo "Fault string: {$fault->faultstring}" . NEWLINE;
	if($soapClient != null)
	{
		$soapClient = null;
	}
	exit();
}
$soapClient = null; 

/*
foreach ($result->GetGroupsResult->SmallGroups as &$SmallGroup) {
	echo NEWLINE;
	echo NEWLINE;
    echo $SmallGroup->CommunityID;
	echo NEWLINE;
	echo $SmallGroup->CommunityName;
	echo NEWLINE;
	echo $SmallGroup->CommunityType;
	echo NEWLINE;
	echo $SmallGroup->EmailList;
	echo NEWLINE;
	echo $SmallGroup->Latitude;
	echo NEWLINE;
	echo $SmallGroup->Location;
	echo NEWLINE;
	echo $SmallGroup->Longitude;
	echo NEWLINE;
	echo $SmallGroup->MeetingDay;
	echo NEWLINE;
	echo $SmallGroup->OpenClosed;
	echo NEWLINE;
	echo $SmallGroup->Studying;
}
*/

?>