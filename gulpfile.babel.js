import gulp from 'gulp';
import requireDir from 'require-dir';

requireDir('./gulp-tasks');

/**
 * Gulp task to run all JavaScript processes in a sequenctial order.
 *
 * @author  Allen Moore
 * @param   {String}   'js' the task name.
 * @param   {Function} cb   the pipe sequence that gulp should run.
 * @returns {void}
 */
gulp.task('js', gulp.series('javascript', function(done) {
  done();
}));

/**
 * Gulp task to run all JavaScript processes in a sequenctial order.
 *
 * @author  Allen Moore
 * @param   {String}   'js' the task name.
 * @param   {Function} cb   the pipe sequence that gulp should run.
 * @returns {void}
 */
gulp.task('copy-js', gulp.series('copy', function(done) {
  done();
}));

/**
 * Gulp task to run all minification processes in a sequencial order.
 *
 * @author  Allen Moore
 * @param   {String}   'minify' the task name.
 * @param   {Function} cb       the pipe sequence that gulp should run.
 * @returns {void}
 */
gulp.task('minify', gulp.series('uglify', function(done) {
  done();
}));

/**
 * Gulp task to watch for file changes and run the associated processes.
 *
 * @author  Allen Moore
 * @param   {String}   'watch' the task name.
 * @returns {void}
 */
gulp.task('watch', () => {
  gulp.watch('./assets/js/**/*.js', ['js', 'copy-js', 'uglify']);
});

/**
 * Gulp task to run the default build processes in a sequenctial order.
 *
 * @author  Allen Moore
 * @param   {String}   'default' the task name.
 * @param   {Function} cb        the pipe sequence that gulp should run.
 * @returns {void}
 */
gulp.task('default', gulp.series(['js', 'copy-js', 'minify'], function(done) {
  done();
}));
