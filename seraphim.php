<?php
/*
Template Name: Seraphim
*/
?>
<?php 
global $wp_query;
$id = $wp_query->get_queried_object_id();
$sidebar = get_post_meta($id, "qode_show-sidebar", true);  

$enable_page_comments = false;
if(get_post_meta($id, "qode_enable-page-comments", true) == 'yes') {
	$enable_page_comments = true;
}

if(get_post_meta($id, "qode_page_background_color", true) != ""){
	$background_color = get_post_meta($id, "qode_page_background_color", true);
}else{
	$background_color = "";
}

$content_style_spacing = "";
if(get_post_meta($id, "qode_margin_after_title", true) != ""){
	if(get_post_meta($id, "qode_margin_after_title_mobile", true) == 'yes'){
		$content_style_spacing = "padding-top:".esc_attr(get_post_meta($id, "qode_margin_after_title", true))."px !important";
	}else{
		$content_style_spacing = "padding-top:".esc_attr(get_post_meta($id, "qode_margin_after_title", true))."px";
	}
}

if ( get_query_var('paged') ) { $paged = get_query_var('paged'); }
elseif ( get_query_var('page') ) { $paged = get_query_var('page'); }
else { $paged = 1; }

?>
	<?php get_header(); ?>
		<?php if(get_post_meta($id, "qode_page_scroll_amount_for_sticky", true)) { ?>
			<script>
			var page_scroll_amount_for_sticky = <?php echo get_post_meta($id, "qode_page_scroll_amount_for_sticky", true); ?>;
			</script>
		<?php } ?>
		<?php get_template_part( 'title' ); ?>
		<?php
		$revslider = get_post_meta($id, "qode_revolution-slider", true);
		if (!empty($revslider)) { ?>
			<div class="q_slider">
				<div class="q_slider_inner">
					<?php echo do_shortcode($revslider); ?>
				</div>
			</div>
		<?php } ?>
		<div class="container"<?php if($background_color != "") { echo " style='background-color:". $background_color ."'";} ?>>
            <?php if(isset($qode_options_proya['overlapping_content']) && $qode_options_proya['overlapping_content'] == 'yes') {?>
                <div class="overlapping_content"><div class="overlapping_content_inner">
            <?php } ?>
			<div class="container_inner default_template_holder clearfix page_container_inner" <?php qode_inline_style($content_style_spacing); ?>>
				<?php if(($sidebar == "default")||($sidebar == "")) : ?>
					<?php if (have_posts()) : 
							while (have_posts()) : the_post(); ?>
							<?php if (get_the_title() != "Messages") { ?>
								<div class="breadcrumb" <?php print $page_title_breadcrumbs_animation_data; ?>><?php qode_custom_breadcrumbs('',''); ?></div>
							<?php } ?>
								
							<?php the_content(); ?>

							<!-- ADD SERAPHIM HERE -->
							<?php require_once (ABSPATH . '/wp-content/themes/the-summit-church-child/seraphim/get-small-groups.php'); ?>
							
							<?php foreach ($result->GetGroupsResult->SmallGroups as $index => &$SmallGroup) {
								
								if ($index % 3 === 0) { ?>
									
									<div class="vc_row wpb_row section vc_row-fluid starting-point" style=" text-align:left;">
									<div class=" full_section_inner clearfix">
								
								<?php } ?>
							
									<div class="wpb_column vc_column_container vc_col-sm-4">
										<div class="vc_column-inner ">
											<div class="wpb_wrapper">
												
												<?php echo '<div class="wpb_text_column wpb_content_element"><div class="wpb_wrapper"><h4>' . $SmallGroup->CommunityName . '</h4></div></div>';
												
												echo '<p>';
													echo '<b>Group Type:</b> ' . $SmallGroup->CommunityType . '<br />';
													echo '<b>Location:</b> ' . $SmallGroup->Location . '<br />';
													echo '<b>Studying:</b> ' . $SmallGroup->Studying . '<br />';
													echo '<b>Meets:</b> ' . $SmallGroup->MeetingDay . '<br />';
													echo '<b>Moderated:</b> ' . $SmallGroup->Moderated . '<br />';
													echo '<b>Open:</b> ' . $SmallGroup->OpenClosed . '</p>';
													
													echo '<div class="widget the_city_plaza_widget">
													<ul class="tc_wp_content">
														<li class="tc_wp_item">
															<a id="time" href="mailto:' . $SmallGroup->EmailList .'" class="qbutton small white" style="white-space: nowrap;"><span class="tc_wp_date dater4">Contact Leader</span></a>
														</li>
														<li class="tc_wp_item">
															<a id="time" href="http://www.google.com/maps/place/' . $SmallGroup->Latitude . ',' . $SmallGroup->Longitude . '" target="_blank" class="qbutton small white" style="white-space: nowrap;"><span class="tc_wp_date dater4">Directions</span></a>
														</li>
													</ul></div>';
													
												?>
																						
											</div>
										</div>
									</div>
								
								<?php if ($index % 3 === 2) { ?>
								
									</div>
									</div>
								
								<?php } ?>
							
							<?php } ?>

							<?php 
								$args_pages = array(
									'before'           => '<p class="single_links_pages">',
									'after'            => '</p>',
									'pagelink'         => '<span>%</span>'
								);
								wp_link_pages($args_pages);
							?>
							<?php
							if($enable_page_comments){
								comments_template('', true); 
							}
							?> 
							<?php endwhile; ?>
						<?php endif; ?>
				<?php elseif($sidebar == "1" || $sidebar == "2"): ?>		
					
					<?php if($sidebar == "1") : ?>	
						<div class="two_columns_66_33 background_color_sidebar grid2 clearfix">
							<div class="column1">
					<?php elseif($sidebar == "2") : ?>	
						<div class="two_columns_75_25 background_color_sidebar grid2 clearfix">
							<div class="column1">
					<?php endif; ?>
							<?php if (have_posts()) : 
								while (have_posts()) : the_post(); ?>
								<div class="column_inner">
								
								<?php the_content(); ?>
								<?php 
									$args_pages = array(
									'before'           => '<p class="single_links_pages">',
									'after'            => '</p>',
									'pagelink'         => '<span>%</span>'
									);

									wp_link_pages($args_pages);
								?>
								<?php
								if($enable_page_comments){
									comments_template('', true); 
								}
								?> 
								</div>
						<?php endwhile; ?>
						<?php endif; ?>
					
									
							</div>
							<div class="column2"><?php get_sidebar();?></div>
						</div>
					<?php elseif($sidebar == "3" || $sidebar == "4"): ?>
						<?php if($sidebar == "3") : ?>	
							<div class="two_columns_33_66 background_color_sidebar grid2 clearfix">
								<div class="column1"><?php get_sidebar();?></div>
								<div class="column2">
						<?php elseif($sidebar == "4") : ?>	
							<div class="two_columns_25_75 background_color_sidebar grid2 clearfix">
								<div class="column1"><?php get_sidebar();?></div>
								<div class="column2">
						<?php endif; ?>
								<?php if (have_posts()) : 
									while (have_posts()) : the_post(); ?>
									<div class="column_inner">
										<?php the_content(); ?>
										<?php 
											$args_pages = array(
												'before'           => '<p class="single_links_pages">',
												'after'            => '</p>',
												'pagelink'         => '<span>%</span>'
											);
											wp_link_pages($args_pages);
										?>
										<?php
										if($enable_page_comments){
											comments_template('', true); 
										}
										?> 
									</div>
							<?php endwhile; ?>
							<?php endif; ?>
						
										
								</div>
								
							</div>
					<?php endif; ?>
			
		</div>
        <?php if(isset($qode_options_proya['overlapping_content']) && $qode_options_proya['overlapping_content'] == 'yes') {?>
            </div></div>
        <?php } ?>
	</div>
	<?php get_footer(); ?>