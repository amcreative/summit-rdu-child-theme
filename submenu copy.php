<?php
	
	$tertiary = false;
	
	// list top-level page IDs to exclude here
	
	$multiply_local = '3433';
	$multiply_live = '3541';
	$multiply_commit_online_local = '3444';
	$multiply_commit_online_live = '3543';
	$multiply_commitment_mod = '4944';
	$multiply_generosity_ladder_local = '3438';
	$multiply_generosity_ladder_live = '3542';
	
	$worship_development = '6904';
		$worship_about = '6932';
		$worship_residency = '6978';
		$worship_internship = '6984';
	
	$about = '215';
		$leadership = '223';
		$stories = '225';
		$pastorjd = '276';
			
	$campus = '16';
		
	$connect = '238';
	
		$starting_point = '217';
		$starting_point_book = '3019';
		$serve = '3017';
	
		$outreach = '256';
			$missions = '2232';
				$missions_local = '258';
					$missions_local_get_involved = '4428';
					$missions_local_about = '2220';
					$missions_local_dwell = '2224';
					$missions_local_blog = '2226';
				$missions_north_america = '1966';
				$missions_international = '1968';
					$missions_international_go_short_term = '2271';
					$missions_international_go_mid_term = '2279';
					$missions_international_go_long_term = '2282';
				$missions_upcoming_trips = '260';
				$missions_pray = '2315';
				$missions_serve365 = '262';
				$missions_church_planting = '264';
				$missions_plumblines = '2329';
				$missions_launch_guide = '7852';
				$missions_launch_guide2 = '7840';
				$missions_why_we_go = '2341';
				$missions_advocacy = '7041';
			
		$college = '244';
			$college_about = '1974';
			$college_staff = '2016';
			$college_get_connected = '2099';
			$college_go_now = '2146';
			$college_upcoming_events = '2148';
			$college_resources = '2153';
		
		$families = '7733';
			$families_phases = '7737';
			$families_milestones = '7744';
			$families_resources = '7746';
			$families_family_plan = '7767';
		
		$students = '1989';
			$middle_school = '4286';
			$high_school = '4306';
		
		$kids = '242';
			$kids_weekend = '2209';
			$kids_resources = '2212';
			$kids_events = '2214';
			$kids_volunteer = '2217';
		
		$orphan_care = '4487';
			
		$give = '13';
			$give_faq = '1658';
			$new_recurring_gift = '4244';
			$new_recurring_gift_local = '3743';
			$cancel_recurring_gift = '4245';
			$cancel_recurring_gift_local = '3762';
			$stewardship = '1642';
	
	$resources = '266';
			
		$counseling = '1958';
			$counseling_graduate_intern = '2410';
			$counseling_bridgehaven = '2418';
			$counseling_pfm = '2421';
			$counseling_freedom_groups = '2437';
			$counseling_g4_groups = '6302';
			$counseling_workshop = '4822';
			$counseling_restoration = '4861';
			$counseling_financial_coaching = '5644';
			$counseling_serve_financial_coach = '5652';
		
		$prayer_bible_study = '1670';
			$bible_reading_plan = '2964';
			$prayer = '2966';
			$pray_for_our_world = '2979';
		
		$books_curriculum = '1502';
			$books_curriculum_jd_greear = '1507';
			$books_curriculum_summit_pastoral_team = '3272';
			$books_curriculum_summit_worship = '3280';
			
	
	// Page Specific In/Excludes
	
	// Multiply
	if ( $post->ID == $multiply_local || $post->post_parent == $multiply_local || $post->ID == $multiply_live || $post->post_parent == $multiply_live ) {
		
		$includes_array = '';
		$excludes_array = $multiply_commitment_mod.', '.$multiply_generosity_ladder_local.', '.$multiply_generosity_ladder_live;
	
	// Starting Point
	} else if ( $post->ID == $starting_point ||
				$post->ID == $starting_point_book ||
				$post->ID == $serve
		) {
		
		$includes_array = $starting_point.', '.$serve;
		$excludes_array = '';
	
	// Worship Development
	} else if ( $post->ID == $worship_development || $post->post_parent == $worship_development || $post->post_parent == $worship_residency || $post->post_parent == $worship_internship ) {
		
		$includes_array = $worship_about.', '.$worship_residency.', '.$worship_internship;
		$excludes_array = '';
	
	// Leadership
	} else if ( $post->post_parent == $leadership ) {
		
		$includes_array = '';
		$excludes_array = '';
		$tertiary = true;
	
	// Missions - Local Outreach
	} else if ( $post->post_parent == $missions_local ) {
		
		$includes_array = '';
		$excludes_array = '';
		$tertiary = true;

	// Missions
	} else if (	$post->ID == $missions || 
				$post->ID == $missions_north_america || 
				$post->ID == $missions_international || 
				$post->ID == $missions_international_go_short_term || 
				$post->ID == $missions_international_go_mid_term || 
				$post->ID == $missions_international_go_long_term || 
 				$post->ID == $missions_upcoming_trips || 
 				$post->ID == $missions_pray || 
				$post->ID == $missions_local || 
				$post->post_parent == $missions_local || 
				$post->post_parent == $missions_international
		) {
		
		$includes_array = '';
		$excludes_array = $missions.', '.$missions_serve365.', '.$missions_church_planting.', '.$missions_plumblines.', '.$missions_launch_guide.', '.$missions_launch_guide2.', '.$missions_why_we_go.', '.$missions_advocacy;

	// College
	} else if ( $post->ID == $college ) {
		
		$includes_array = '';//$college_about.', '.$college_staff.', '.$college_get_connected.', '.$college_go_now.', '.$college_upcoming_events.', '.$college_resources;
		$excludes_array = '';
		$tertiary = true;

	// College Subpages
	} else if (	$post->post_parent == $college ) {
		
		$includes_array = '';//$college_about.', '.$college_staff.', '.$college_get_connected.', '.$college_go_now.', '.$college_upcoming_events.', '.$college_resources;
		$excludes_array = '';
		$tertiary = true;
	
	// Family Ministries Subpages
	} else if (	$post->ID == $families || $post->post_parent == $families ) {
		
		$includes_array = '';
		$excludes_array = '';
		$tertiary = true;

	// Students Subsections
	} else if (	$post->post_parent == $middle_school || $post->post_parent == $high_school ) {
		
		$includes_array = '';
		$excludes_array = '';
		$tertiary = true;

	// Kids Subpages
	} else if (	$post->post_parent == $kids ) {
		
		$includes_array = '';
		$excludes_array = '';
		$tertiary = true;

	// Orphan Care Subpages
	} else if (	$post->post_parent == $orphan_care ) {
		
		$includes_array = '';
		$excludes_array = '';
		$tertiary = true;

	// Give
	} else if ( $post->post_parent == $give ) {
		
		$includes_array = '';
		$excludes_array = $give_faq.', '.$new_recurring_gift.', '.$new_recurring_gift_local.', '.$cancel_recurring_gift.', '.$cancel_recurring_gift_local;
		$tertiary = true;

	// Stewardship
	} else if ( $post->post_parent == $stewardship ) {
		
		$includes_array = '';
		$excludes_array = $give_faq.', '.$new_recurring_gift.', '.$new_recurring_gift_local.', '.$cancel_recurring_gift.', '.$cancel_recurring_gift_local;

	// Counseling
	} else if ($post->ID == $counseling) {
		
		$includes_array = $counseling_graduate_intern.', '.$counseling_bridgehaven.', '.$counseling_pfm.', '.$counseling_g4_groups.', '.$counseling_financial_coaching;
		$excludes_array = '';

	// Counseling Subpages
	} else if ( $post->post_parent == $counseling ) {
		
		$includes_array = '';
		$excludes_array = $counseling_workshop.', '.$counseling_restoration.', '.$counseling_freedom_groups.', '.$counseling_serve_financial_coach;
		$tertiary = true;

	// Prayer & Bible Study Subpages
	} else if ( $post->post_parent == $prayer_bible_study ) {
		
		$includes_array = '';
		$excludes_array = '';
		$tertiary = true;
	
	// Books & Curriculum
	} else if ( $post->post_parent == $books_curriculum ) {
		
		$includes_array = '';
		$excludes_array = '';
		$tertiary = true;
		
	// Global
	} else {
		
		$includes_array = '';
		$excludes_array = $starting_point.', '.$stories.', '.$connect.', '.$campus.', '.$resources;
		
	}
		
	// builder strings
	$child_pages_includes = 'include=' . $includes_array;
	$child_pages_excludes = '&exclude_tree=' . $excludes_array;
	
	if ($post->ancestors[1] && $tertiary == false) {
		$child_pages_attr = '&title_li=&child_of=' . $post->ancestors[1] . '&echo=0&depth=2';
	} else if ($post->ancestors[1] && $tertiary == true) {
		$child_pages_attr = '&title_li=&child_of=' . $post->post_parent . '&echo=0&depth=2';
	} else if ($tertiary == true) {
		$child_pages_attr = '&title_li=&child_of=' . $post->ID . '&echo=0&depth=2';
	} else {
		$child_pages_attr = '&title_li=&child_of=' . $post->post_parent . '&echo=0&depth=2';
	}
	
	// subnav items
	if ($post->post_parent) {
		
		// The child pages of the Top-Level Parent Page
		$children = wp_list_pages( $child_pages_includes . $child_pages_excludes . $child_pages_attr );
		
	} else {
		
		// The Top-Level Parent Page (Never Shown)
		$children = wp_list_pages( $child_pages_includes . $child_pages_excludes . '&title_li=&child_of=' . $post->ID . '&echo=0&depth=1');
		
	}
	
	
	// subnav structure
	if ($children) { ?>
			
		<subnav>
			
			<div class="mobile_subnav_holder"><a href="#">Sub Menu <i class="fa fa-angle-down open" style=""></i><i class="fa fa-angle-up close" style=""></i></a></div>
	
			<div class="subnav_container"><ul>
				
				<?php echo $children; ?>
				
			</ul></div>
	
		</subnav>
		
<?php } ?>