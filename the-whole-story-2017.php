<?php
/*
Template Name: The Whole Story 2017
*/
?>
<?php 
global $wp_query;
$id = $wp_query->get_queried_object_id();
$sidebar = get_post_meta($id, "qode_show-sidebar", true);  

$enable_page_comments = false;
if(get_post_meta($id, "qode_enable-page-comments", true) == 'yes') {
	$enable_page_comments = true;
}

if(get_post_meta($id, "qode_page_background_color", true) != ""){
	$background_color = get_post_meta($id, "qode_page_background_color", true);
}else{
	$background_color = "";
}

$content_style_spacing = "";
if(get_post_meta($id, "qode_margin_after_title", true) != ""){
	if(get_post_meta($id, "qode_margin_after_title_mobile", true) == 'yes'){
		$content_style_spacing = "padding-top:".esc_attr(get_post_meta($id, "qode_margin_after_title", true))."px !important";
	}else{
		$content_style_spacing = "padding-top:".esc_attr(get_post_meta($id, "qode_margin_after_title", true))."px";
	}
}

if ( get_query_var('paged') ) { $paged = get_query_var('paged'); }
elseif ( get_query_var('page') ) { $paged = get_query_var('page'); }
else { $paged = 1; }

?>
	<?php get_header(); ?>
		<?php if(get_post_meta($id, "qode_page_scroll_amount_for_sticky", true)) { ?>
			<script>
			var page_scroll_amount_for_sticky = <?php echo get_post_meta($id, "qode_page_scroll_amount_for_sticky", true); ?>;
			</script>
		<?php } ?>
			<?php get_template_part( 'title' ); ?>
		<?php
		$revslider = get_post_meta($id, "qode_revolution-slider", true);
		if (!empty($revslider)){ ?>
			<div class="q_slider">
				<div class="q_slider_inner">
					<?php echo do_shortcode($revslider); ?>
				</div>
			</div>
		<?php } ?>
		<div class="full_width"<?php if($background_color != "") { echo " style='background-color:". $background_color ."'";} ?>>
        <?php if(isset($qode_options_proya['overlapping_content']) && $qode_options_proya['overlapping_content'] == 'yes') {?>
            <div class="overlapping_content"><div class="overlapping_content_inner">
        <?php } ?>
		<div class="full_width_inner" <?php qode_inline_style($content_style_spacing); ?>>
		<?php if(($sidebar == "default")||($sidebar == "")) : ?>
			<?php if (have_posts()) : 
					while (have_posts()) : the_post(); ?>
					<div class="vc_row wpb_row section vc_row-fluid grid_section full_width" style="text-align:left;">
						<div class="section_inner clearfix">
							<div class="section_inner_margin clearfix">
								<div class="breadcrumb" <?php print $page_title_breadcrumbs_animation_data; ?>> <?php qode_custom_breadcrumbs('',''); ?></div>
							</div>
						</div>
					</div>
					<?php the_content(); ?>

					<div class="vc_row wpb_row section vc_row-fluid grid_section bible_container" style="text-align: left;">
						<div class=" section_inner clearfix">
							<div class="section_inner_margin clearfix">
								
								<div class="wpb_column vc_column_container vc_col-xs-12 vc_col-sm-6">
									<div class="wpb_wrapper">
										<div class="wpb_text_column wpb_content_element  wpb_animate_when_almost_visible wpb_ wpb_start_animation">
											<div class="wpb_wrapper">
												<h3 style="text-align: center;">Bible in a Year</h3>
											</div>
										</div>
										<div class="wpb_text_column wpb_content_element wpb_animate_when_almost_visible wpb_ wpb_start_animation">
											<div class="wpb_wrapper">
												
												<?php
													
													date_default_timezone_set('America/New_York');
													$startDate = date('D, M j', strtotime('this week')); //'2016-01-04';
													$todayDate = date('D, M j');
													$weekDate = new DateTime($todayDate);
													$weekNum = $weekDate->format('W');
													$weekNumCap = $weekNum * 7;
													//$weekCounter = 0;
													
													/* The Whole Story Readings
													============================================ */
																    	
											    	$url = "http://www.summitrdu.com/wp-content/themes/the-summit-church-child/bible/the-whole-story.xml";
													$xml = simplexml_load_file($url);
													$counter = 0;
													//echo '<pre>',print_r($xml),'</pre>';
													//echo $xml->reading[0]->passage;
													
													foreach ($xml->reading as $item):
														
														/*
														echo $weekNum,'<br>'; // 01
														echo $weekNumCap,'<br>'; // 7
														echo $weekCounter,'<br>'; // 0
														echo $weekNumCap-1,'<br>'; // 6
														echo $counter,'<br>'; // 0
														echo (7 * ($weekNum-1)) + $counter,'<br>'; // 0
														echo '<br>';
														*/
														
														if ($counter == $weekNumCap) { break; }
														if ($weekCounter == ($weekNumCap-1)) { break; }
														$weekCounter = (7 * ($weekNum-1)) + $counter;
														if ($counter >= $weekCounter) { continue; }
														
													    $date = date('D, M j', strtotime('+'.$counter.' day', strtotime($startDate)));
														$reading = $xml->reading[$weekCounter]->passage;
														
														$wpb_column_container = '<div class="wpb_column vc_column_container vc_col-sm-12"';
														$wpb_column_wrapper = '<div class="wpb_wrapper">
																					<div class="wpb_text_column wpb_content_element">
																						<div class="wpb_wrapper">';
																						
														$wpb_column_end = '				</div>
																					</div>
																				</div>
																			</div>';
														
														echo '<div class="vc_row wpb_row section vc_row-fluid bible_wrapper" style="text-align:left;">
																<div class="full_section_inner clearfix">';
													    
													    if ($date == $todayDate) {
														    echo $wpb_column_container . ' id="todaysReading">';
														    echo $wpb_column_wrapper;
													    } else {
														    echo $wpb_column_container . '>';
														    echo $wpb_column_wrapper;
													    }
													    
													    if ($reading == "Reflection") {
														    echo '<h3><i class="fa fa-calendar-o"></i>';
													    } else {
														    echo '<h3><i class="fa fa-calendar-o"></i><a href="http://classic.biblegateway.com/passage/?search=', $reading ,'&version=ESV&interface=print" target="_blank">';
													    }
													    echo $date;
														if ($reading == "Reflection") { echo '</h3>'; } else { echo '</a></h3>'; }
														echo '<p>', $reading,'</p>';
														echo $wpb_column_end;
														echo '</div></div>';
													
														$counter++;
													
													endforeach;
													
												?>
									
											</div>
										</div>
									</div>
								</div>
									
								<div class="wpb_column vc_column_container vc_col-xs-12 vc_col-sm-6">
									<div class="wpb_wrapper">
										<div class="wpb_text_column wpb_content_element  wpb_animate_when_almost_visible wpb_ wpb_start_animation">
											<div class="wpb_wrapper">
												<h3 style="text-align: center;">Genre Plan 2017</h3>
											</div>
										</div>
										<div class="wpb_text_column wpb_content_element wpb_animate_when_almost_visible wpb_ wpb_start_animation">
											<div class="wpb_wrapper">
												
												<?php
													
													/* Genre Edition Readings
													============================================ */
																    	
											    	$url2 = "http://www.summitrdu.com/wp-content/themes/the-summit-church-child/bible/genre-edition.xml";
													$xml2 = simplexml_load_file($url2);
													echo $xml2;
													$counter2 = 0;
													$weekCounter2 = 0;
													
													foreach ($xml2->reading as $item2):
														
														if ($counter2 == $weekNumCap) { break; }
														if ($weekCounter2 == ($weekNumCap-1)) { break; }
														$weekCounter2 = (7 * ($weekNum-1)) + $counter2;
														if ($counter2 >= $weekCounter2) { continue; }
														
													    $date2 = date('D, M j', strtotime('+'.$counter2.' day', strtotime($startDate)));
														$reading2 = $xml2->reading[$weekCounter2]->passage;
														
														$wpb_column_container2 = '<div class="wpb_column vc_column_container vc_col-sm-12"';
														$wpb_column_wrapper2 = '<div class="wpb_wrapper">
																					<div class="wpb_text_column wpb_content_element">
																						<div class="wpb_wrapper">';
																						
														$wpb_column_end2 = '				</div>
																					</div>
																				</div>
																			</div>';
														
														echo '<div class="vc_row wpb_row section vc_row-fluid bible_wrapper" style="text-align:left;">
																<div class="full_section_inner clearfix">';
													    
													    if ($date2 == $todayDate) {
														    echo $wpb_column_container2 . ' id="todaysReading">';
														    echo $wpb_column_wrapper2;
													    } else {
														    echo $wpb_column_container2 . '>';
														    echo $wpb_column_wrapper2;
													    }
													    
													    if ($reading2 == "Reflection") {
														    echo '<h3><i class="fa fa-calendar-o"></i>';
													    } else {
														    echo '<h3><i class="fa fa-calendar-o"></i><a href="http://classic.biblegateway.com/passage/?search=', $reading2 ,'&version=ESV&interface=print" target="_blank">';
													    }
													    echo $date2;
														if ($reading2 == "Reflection") { echo '</h3>'; } else { echo '</a></h3>'; }
														echo '<p>', $reading2,'</p>';
														echo $wpb_column_end2;
														echo '</div></div>';
													
														$counter2++;
													
													endforeach;
													
												?>
									
											</div>
										</div>
									</div>
								</div>
																
							</div>
						</div>
					</div>
					
					<?php 
					 $args_pages = array(
					  'before'           => '<p class="single_links_pages">',
					  'after'            => '</p>',
					  'pagelink'         => '<span>%</span>'
					 );
					
					 wp_link_pages($args_pages); ?>
					<?php
					if($enable_page_comments){
					?>
					<div class="container">
						<div class="container_inner">
					<?php
						comments_template('', true); 
					?>
						</div>
					</div>	
					<?php
					}
					?> 
					<?php endwhile; ?>
				<?php endif; ?>
		<?php elseif($sidebar == "1" || $sidebar == "2"): ?>		
			
			<?php if($sidebar == "1") : ?>	
				<div class="two_columns_66_33 clearfix grid2">
					<div class="column1">
			<?php elseif($sidebar == "2") : ?>	
				<div class="two_columns_75_25 clearfix grid2">
					<div class="column1">
			<?php endif; ?>
					<?php if (have_posts()) : 
						while (have_posts()) : the_post(); ?>
						<div class="column_inner">
						
						<?php the_content(); ?>	
						<?php 
 $args_pages = array(
  'before'           => '<p class="single_links_pages">',
  'after'            => '</p>',
  'pagelink'         => '<span>%</span>'
 );

 wp_link_pages($args_pages); ?>
							<?php
							if($enable_page_comments){
							?>
							<div class="container">
								<div class="container_inner">
							<?php
								comments_template('', true); 
							?>
								</div>
							</div>	
							<?php
							}
							?> 
						</div>
				<?php endwhile; ?>
				<?php endif; ?>
			
							
					</div>
					<div class="column2"><?php get_sidebar();?></div>
				</div>
			<?php elseif($sidebar == "3" || $sidebar == "4"): ?>
				<?php if($sidebar == "3") : ?>	
					<div class="two_columns_33_66 clearfix grid2">
						<div class="column1"><?php get_sidebar();?></div>
						<div class="column2">
				<?php elseif($sidebar == "4") : ?>	
					<div class="two_columns_25_75 clearfix grid2">
						<div class="column1"><?php get_sidebar();?></div>
						<div class="column2">
				<?php endif; ?>
						<?php if (have_posts()) : 
							while (have_posts()) : the_post(); ?>
							<div class="column_inner">
							<?php the_content(); ?>		
							<?php 
 $args_pages = array(
  'before'           => '<p class="single_links_pages">',
  'after'            => '</p>',
  'pagelink'         => '<span>%</span>'
 );

 wp_link_pages($args_pages); ?>
							<?php
							if($enable_page_comments){
							?>
							<div class="container">
								<div class="container_inner">
							<?php
								comments_template('', true); 
							?>
								</div>
							</div>	
							<?php
							}
							?> 
							</div>
					<?php endwhile; ?>
					<?php endif; ?>
				
								
						</div>
						
					</div>
			<?php endif; ?>
	</div>
	</div>	
	<?php get_footer(); ?>