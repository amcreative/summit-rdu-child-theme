<?php 
/*
Template Name: Redirect to First Child
*/ 
?>
<?php
	// Redirect Parent Pages to first Child Page if exists
	$pageChild = get_pages("child_of=".$post->ID."&sort_column=menu_order");
	if ($pageChild) {
		$firstChild = $pageChild[0];
		wp_redirect(get_permalink($firstChild->ID));
	} else {
		// else redirect to homepage
		wp_redirect(get_home_url());
	}
?>