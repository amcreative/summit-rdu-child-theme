<?php
/*
Template Name: Bible Reading Plan
*/
?>
<?php 
global $wp_query;
$id = $wp_query->get_queried_object_id();
$sidebar = get_post_meta($id, "qode_show-sidebar", true);  

$enable_page_comments = false;
if(get_post_meta($id, "qode_enable-page-comments", true) == 'yes') {
	$enable_page_comments = true;
}

if(get_post_meta($id, "qode_page_background_color", true) != ""){
	$background_color = get_post_meta($id, "qode_page_background_color", true);
}else{
	$background_color = "";
}

$content_style_spacing = "";
if(get_post_meta($id, "qode_margin_after_title", true) != ""){
	if(get_post_meta($id, "qode_margin_after_title_mobile", true) == 'yes'){
		$content_style_spacing = "padding-top:".esc_attr(get_post_meta($id, "qode_margin_after_title", true))."px !important";
	}else{
		$content_style_spacing = "padding-top:".esc_attr(get_post_meta($id, "qode_margin_after_title", true))."px";
	}
}

if ( get_query_var('paged') ) { $paged = get_query_var('paged'); }
elseif ( get_query_var('page') ) { $paged = get_query_var('page'); }
else { $paged = 1; }

?>
	<?php get_header(); ?>
		<?php if(get_post_meta($id, "qode_page_scroll_amount_for_sticky", true)) { ?>
			<script>
			var page_scroll_amount_for_sticky = <?php echo get_post_meta($id, "qode_page_scroll_amount_for_sticky", true); ?>;
			</script>
		<?php } ?>
			<?php get_template_part( 'title' ); ?>
		<?php
		$revslider = get_post_meta($id, "qode_revolution-slider", true);
		if (!empty($revslider)){ ?>
			<div class="q_slider">
				<div class="q_slider_inner">
					<?php echo do_shortcode($revslider); ?>
				</div>
			</div>
		<?php } ?>
		<div class="full_width"<?php if($background_color != "") { echo " style='background-color:". $background_color ."'";} ?>>
        <?php if(isset($qode_options_proya['overlapping_content']) && $qode_options_proya['overlapping_content'] == 'yes') {?>
            <div class="overlapping_content"><div class="overlapping_content_inner">
        <?php } ?>
		<div class="full_width_inner" <?php qode_inline_style($content_style_spacing); ?>>
		<?php if(($sidebar == "default")||($sidebar == "")) : ?>
			<?php if (have_posts()) : 
					while (have_posts()) : the_post(); ?>
					<div class="vc_row wpb_row section vc_row-fluid grid_section full_width" style="text-align:left;">
						<div class="section_inner clearfix">
							<div class="section_inner_margin clearfix">
								<div class="breadcrumb" <?php print $page_title_breadcrumbs_animation_data; ?>> <?php qode_custom_breadcrumbs('',''); ?></div>
							</div>
						</div>
					</div>
					<?php the_content(); ?>
					
					<?php
									    	
				    	$url = "http://www.oneyearbibleonline.com/rss/oneyearbibleonlinereadings.xml";
						$xml = simplexml_load_file($url);
							
						$todayDate = date('D, M d');
						$todayDate = preg_replace('/\s+/', '', $todayDate);
						$todayDate = str_replace(',', '', $todayDate);
						
						$newtBooks = array('MATTHEW','MARK','LUKE','JOHN','ACTS','ROMANS','1 CORINTHIANS','2 CORINTHIANS','GALATIANS','EPHESIANS','PHILIPPIANS','COLOSSIANS','1 THESSALONIANS','2 THESSALONIANS','1 TIMOTHY','2 TIMOTHY','TITUS','PHILEMON','HEBREWS','JAMES','1 PETER','2 PETER','1 JOHN','2 JOHN','3 JOHN','JUDE','REVELATION');
						

					
					?>


					<div class="vc_row wpb_row section vc_row-fluid grid_section bible_container" style="text-align: left;">
						<div class=" section_inner clearfix">
							<div class="section_inner_margin clearfix">
								
								<div class="wpb_column vc_column_container vc_col-sm-12 vc_col-md-6">
									<div class="wpb_wrapper">
										<div class="wpb_text_column wpb_content_element  wpb_animate_when_almost_visible wpb_ wpb_start_animation">
											<div class="wpb_wrapper">
												<h3 style="text-align: center;">Old &amp; New Testament</h3>
											</div>
										</div>
										<div class="wpb_text_column wpb_content_element  wpb_animate_when_almost_visible wpb_ wpb_start_animation">
											<div class="wpb_wrapper">
												
												<!-- Old & New Testament Here -->
												
												<?php
													
													foreach ($xml->item as $item):
													    $title = $item->title;
													    $desc = $item->description;
													    $link = $item->link;
													    $readingA = $item->readinga;
													    $readingB = $item->readingb;
													    $readingC = $item->readingc;
													    $readingD = $item->readingd;
													    
													    
													    /* Old & New Testament Readings
														============================================ */
													    
													    $verseDate = $title;
														$verseDate = preg_replace('/\s+/', '', $verseDate);
														$verseID = str_replace(',', '', $verseDate);
														
														$wpb_column_container = '<div class="wpb_column vc_column_container vc_col-sm-12"';
														$wpb_column_wrapper = '<div class="wpb_wrapper">
																					<div class="wpb_text_column wpb_content_element">
																						<div class="wpb_wrapper">';
																						
														$wpb_column_end = '				</div>
																					</div>
																				</div>
																			</div>';

														
														echo '<div class="vc_row wpb_row section vc_row-fluid bible_wrapper" style="text-align:left;">
																<div class="full_section_inner clearfix">';
													    
													    if ($verseID == $todayDate) {
														    echo $wpb_column_container . ' id="todaysReading">';
														    echo $wpb_column_wrapper;
													    } else {
														    echo $wpb_column_container . '>';
														    echo $wpb_column_wrapper;
													    }
													    
														echo '<h3><i class="fa fa-calendar-o"></i><a href="', $link ,'&version=ESV&interface=print" target="_blank">', $title ,'</a></h3>';
														echo '<p>', $readingA,', ',$readingB,', ',$readingC,', ',$readingD,'</p>';
														echo $wpb_column_end;
														echo '</div></div>';
														
													endforeach;
													
												?>
									
											</div>
										</div>
									</div>
								</div>
									
								<div class="wpb_column vc_column_container vc_col-sm-12 vc_col-md-6 last">
									<div class="wpb_wrapper">
										<div class="wpb_text_column wpb_content_element  wpb_animate_when_almost_visible wpb_ wpb_start_animation">
											<div class="wpb_wrapper">
												<h3 style="text-align: center;">New Testament</h3>
											</div>
										</div>
										<div class="wpb_text_column wpb_content_element  wpb_animate_when_almost_visible wpb_ wpb_start_animation">
											<div class="wpb_wrapper">
												
												<!-- New Testament Here -->
												
												<?php
													
													foreach ($xml->item as $item):
													    $title = $item->title;
													    $desc = $item->description;
													    $link = $item->link;
													    $readingA = $item->readinga;
													    $readingB = $item->readingb;
													    $readingC = $item->readingc;
													    $readingD = $item->readingd;
													    
													    
													    /* Old & New Testament Readings
														============================================ */
													    
													    $verseDate = $title;
														$verseDate = preg_replace('/\s+/', '', $verseDate);
														$verseID = str_replace(',', '', $verseDate);
														
														$wpb_column_container = '<div class="wpb_column vc_column_container vc_col-sm-12"';
														$wpb_column_wrapper = '<div class="wpb_wrapper">
																					<div class="wpb_text_column wpb_content_element">
																						<div class="wpb_wrapper">';
																						
														$wpb_column_end = '				</div>
																					</div>
																				</div>
																			</div>';

														
														echo '<div class="vc_row wpb_row section vc_row-fluid bible_wrapper" style="text-align:left;">
																<div class="full_section_inner clearfix">';
														
														
														/* New Testament Readings ONLY
														============================================ */
														
														if ($verseID == $todayDate) {
														    echo $wpb_column_container . ' id="todaysReadingNT">';
														    echo $wpb_column_wrapper;
													    } else {
														    echo $wpb_column_container . '>';
														    echo $wpb_column_wrapper;
													    }
													    
													    $readingA = $item->readinga;
													    $readingB = $item->readingb;
													    $readingC = $item->readingc;
													    $readingD = $item->readingd;
													    
													    if (substr_count($readingA, ' ') > 1) {
														    $readingNameA = substr($readingA, 0, strpos($readingA, ' ', strpos($readingA, ' ')+1));
													    } else {
														    $readingNameA = strtok($readingA, ' ');
													    }
													    
													    if (substr_count($readingB, ' ') > 1) {
														    $readingNameB = substr($readingB, 0, strpos($readingB, ' ', strpos($readingB, ' ')+1));
													    } else {
														    $readingNameB = strtok($readingB, ' ');
													    }
													    
													    if (substr_count($readingC, ' ') > 1) {
														    $readingNameC = substr($readingA, 0, strpos($readingC, ' ', strpos($readingC, ' ')+1));
													    } else {
														    $readingNameC = strtok($readingC, ' ');
													    }
													    
													    if (substr_count($readingD, ' ') > 1) {
														    $readingNameD = substr($readingD, 0, strpos($readingD, ' ', strpos($readingD, ' ')+1));
													    } else {
														    $readingNameD = strtok($readingD, ' ');
													    }
													    
													    $readings = array($readingA, $readingB, $readingC, $readingD);
													    $readingNames = array($readingNameA, $readingNameB, $readingNameC, $readingNameD);
													    										
												    	foreach ($readingNames as &$newtBook) {
											    			if (in_array($newtBook, $newtBooks)) {
												    			$key = array_search($newtBook, $readingNames);
															    echo '<h3><i class="fa fa-calendar-o"></i><a href="http://classic.biblegateway.com/passage/?search=', $readings[$key] ,'&version=ESV&interface=print" target="_blank">', $title ,'</a></h3>';
													    		echo '<p>', $readings[$key] ,'</p>';
												    		}
											    		}
											    		echo $wpb_column_end;
														echo '</div></div>';
														
													endforeach;
													
												?>
									
											</div>
										</div>
									</div>
								</div>
								
							</div>
						</div>
					</div>
					
					<?php 
					 $args_pages = array(
					  'before'           => '<p class="single_links_pages">',
					  'after'            => '</p>',
					  'pagelink'         => '<span>%</span>'
					 );
					
					 wp_link_pages($args_pages); ?>
					<?php
					if($enable_page_comments){
					?>
					<div class="container">
						<div class="container_inner">
					<?php
						comments_template('', true); 
					?>
						</div>
					</div>	
					<?php
					}
					?> 
					<?php endwhile; ?>
				<?php endif; ?>
		<?php elseif($sidebar == "1" || $sidebar == "2"): ?>		
			
			<?php if($sidebar == "1") : ?>	
				<div class="two_columns_66_33 clearfix grid2">
					<div class="column1">
			<?php elseif($sidebar == "2") : ?>	
				<div class="two_columns_75_25 clearfix grid2">
					<div class="column1">
			<?php endif; ?>
					<?php if (have_posts()) : 
						while (have_posts()) : the_post(); ?>
						<div class="column_inner">
						
						<?php the_content(); ?>	
						<?php 
 $args_pages = array(
  'before'           => '<p class="single_links_pages">',
  'after'            => '</p>',
  'pagelink'         => '<span>%</span>'
 );

 wp_link_pages($args_pages); ?>
							<?php
							if($enable_page_comments){
							?>
							<div class="container">
								<div class="container_inner">
							<?php
								comments_template('', true); 
							?>
								</div>
							</div>	
							<?php
							}
							?> 
						</div>
				<?php endwhile; ?>
				<?php endif; ?>
			
							
					</div>
					<div class="column2"><?php get_sidebar();?></div>
				</div>
			<?php elseif($sidebar == "3" || $sidebar == "4"): ?>
				<?php if($sidebar == "3") : ?>	
					<div class="two_columns_33_66 clearfix grid2">
						<div class="column1"><?php get_sidebar();?></div>
						<div class="column2">
				<?php elseif($sidebar == "4") : ?>	
					<div class="two_columns_25_75 clearfix grid2">
						<div class="column1"><?php get_sidebar();?></div>
						<div class="column2">
				<?php endif; ?>
						<?php if (have_posts()) : 
							while (have_posts()) : the_post(); ?>
							<div class="column_inner">
							<?php the_content(); ?>		
							<?php 
 $args_pages = array(
  'before'           => '<p class="single_links_pages">',
  'after'            => '</p>',
  'pagelink'         => '<span>%</span>'
 );

 wp_link_pages($args_pages); ?>
							<?php
							if($enable_page_comments){
							?>
							<div class="container">
								<div class="container_inner">
							<?php
								comments_template('', true); 
							?>
								</div>
							</div>	
							<?php
							}
							?> 
							</div>
					<?php endwhile; ?>
					<?php endif; ?>
				
								
						</div>
						
					</div>
			<?php endif; ?>
	</div>
	</div>	
	<?php get_footer(); ?>