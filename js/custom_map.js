var $j = jQuery.noConflict();
var map;
var image = '/wp-content/uploads/2015/10/summit-marker.png';
var template = "<div class='map-overlay'><a class='map-link' href='[URL]'>[TITLE]</a>[CONTENT]</div>";
var markers = {};
var openMarker;

//$j(document).ready(initialize);
$j(window).load(initialize);

function createMarker(lat,lng,title,url,content) {
	var marker = new google.maps.Marker({
		position: new google.maps.LatLng(lat,lng),
// 		animation: google.maps.Animation.DROP,
		title: title,
		map: map,
		icon: image
	});
	var contentwindow=new google.maps.InfoWindow({
		content:template.replace("[TITLE]",title).replace("[URL]",url).replace("[CONTENT]",content)
	}); 
	google.maps.event.addListener(marker, 'click', function() {
		if(openMarker)openMarker.close();
		contentwindow.open(map,marker);
		openMarker=contentwindow;
	});
	return {marker:marker,window:contentwindow};
}

function createDoubleMarker(lat,lng,title,url,content,title2,url2,content2) {
	var marker = new google.maps.Marker({
		position: new google.maps.LatLng(lat,lng),
// 		animation: google.maps.Animation.DROP,
		title: title,
		map: map,
		icon: image
	});
	var contentwindow=new google.maps.InfoWindow({
		content:template.replace("[TITLE]",title).replace("[URL]",url).replace("[CONTENT]",content)+"<br>" +
		    ""+
		template.replace("[TITLE]",title2).replace("[URL]",url2).replace("[CONTENT]",content2)
	}); 
	google.maps.event.addListener(marker, 'click', function() {
		if(openMarker)openMarker.close();
		contentwindow.open(map,marker);
		openMarker=contentwindow;
	});
	return {marker:marker,window:contentwindow};
}

function initialize() {
	
	$j('#map_canvas').fadeIn();
	
	var parts = window.location.toString().split("#");
	var hash = (parts.length>1)?parts[1]:"";
	hash = hash.replace("�","&ntilde;");
	
	var mapStyles = [
	    {
	      stylers: [
					{hue: "#000000" },
					{saturation: "-100"},
					{lightness: "0"},
					{gamma: 1.51}
				]
	    }
	];
	
	var qodeMapType = new google.maps.StyledMapType(mapStyles,
    {name: "Qode Map"});
	
	var myOptions = {
		zoom: 10,
				scrollwheel: false,
				center: new google.maps.LatLng(35.898587, -78.823035),
		zoomControl: true,
		zoomControlOptions: {
			style: google.maps.ZoomControlStyle.SMALL,
			position: google.maps.ControlPosition.RIGHT_CENTER
		},
		scaleControl: false,
			scaleControlOptions: {
			position: google.maps.ControlPosition.LEFT_CENTER
		},
		streetViewControl: false,
			streetViewControlOptions: {
			position: google.maps.ControlPosition.LEFT_CENTER
		},
		panControl: false,
		panControlOptions: {
			position: google.maps.ControlPosition.LEFT_CENTER
		},
		mapTypeControl: false,
		mapTypeControlOptions: {
			mapTypeIds: [google.maps.MapTypeId.ROADMAP, 'qode_style'],
			style: google.maps.MapTypeControlStyle.HORIZONTAL_BAR,
			position: google.maps.ControlPosition.LEFT_CENTER
		},
		mapTypeId: 'qode_style'
	};

	map = new google.maps.Map(document.getElementById('map_canvas'), myOptions);
	map.mapTypes.set('qode_style', qodeMapType);


	setTimeout(function() {
	    try {
		    navigator.geolocation.getCurrentPosition(onPositionSuccess,onPositionFail);
		}
		catch(e) {
			/*Whoops*/
		}
	},0);
    
    setTimeout(function() {
	    markers['alamance-county'] = createMarker(
		    36.0614763, -79.2972038,
		    "Alamance County Campus",
		    "/locations/alamance-county",
		    "Sundays at 9 &amp; 10:45 am<br>" +
		    "<a href='https://goo.gl/maps/Xg6YWs6UV7K2' target='_blank'>Hawfield Middle School<br>" +
			"1951 NC-119<br>" +
			"Mebane, NC 27302</a>"
	    );
    },0);
    
    setTimeout(function() {
	    markers['prison-ministry-mens'] = createMarker(
		    35.7653186, -78.6171517,
		    "Prison Ministry - Men's",
		    "/locations/prison-ministry-mens/",
		    "Sundays at 3:15 pm<br>" +
		    "<a href='https://goo.gl/maps/gk5d54rUjeN2' target='_blank'>Wake Correctional Center<br>" +
			"1000 Rock Quarry Rd<br>" +
			"Raleigh, NC 27610</a>"
	    );
    },0);
    
    setTimeout(function() {
	    markers['prison-ministry-womens'] = createMarker(
		    35.7654258, -78.6208556,
		    "Prison Ministry - Women's",
		    "/locations/prison-ministry-womens/",
		    "Thursdays at 6:30 pm<br>" +
		    "<a href='https://goo.gl/maps/Xd6MHDHCfQD2' target='_blank'>NCCIW<br>" +
			"1034 Bragg St<br>" +
			"Raleigh, NC 27610</a>"
	    );
    },0);
    
    setTimeout(function() {
	    markers['apex'] = createMarker(
		    35.7226205, -78.8227766,
		    "Apex Campus",
		    "/locations/apex",
			"Saturdays at 4 &amp; 5:30 pm<br>" +
		    "Sundays at 9 <i style='background-image: url(/wp-content/themes/the-summit-church-child/images/sign-language-service-dark.png); background-size: 100%; background-repeat: no-repeat; background-position-y: 3px; padding-left: 18px; margin-right: 4px;'></i> &amp; 10:45 am<br>" +
		    "<a href='https://goo.gl/maps/sN7TF3iKzap' target='_blank'>3000 Lufkin Rd<br>" +
			"Apex, NC 27539</a>"
	    );
    },0);
    
    setTimeout(function() {
	    markers['blue-ridge'] = createMarker(
		    35.825505,-78.699909,
		    "Blue Ridge Campus",
		    "/locations/blue-ridge/",
			"Saturdays at 4 &amp; 5:30 pm<br>" +
		    "Sundays at 9 &amp; 10:45 am<br>" +
		    "<a href='http://goo.gl/maps/c1UcYnLgP1S2' target='_blank'>3249 Blue Ridge Rd<br>" +
		    "Raleigh, NC 27612</a>"
	    );
    },0);
    
    setTimeout(function() {
	    markers['brier-creek'] = markers['summit-en-espanol'] = createDoubleMarker(
		    35.926734, -78.841508,
		    "Brier Creek Campus",
		    "/locations/brier-creek",
			"Saturdays at 4 &amp; 5:30 pm<br>" +
		    "Sundays at 9 &amp; 10:45 am<br>" +
		    "<a href='http://goo.gl/maps/H1HVniAnoYs' target='_blank'>2335 Presidential Dr<br>" +
		    "Durham, NC 27703</a><br>","Summit En Espa&ntilde;ol","/locations/summit-en-espanol","Domingos a las 10:45 am<br>" +
		    "<a href='https://goo.gl/maps/NndsEYhnhJS2' target='_blank'>2415 Presidential Dr<br>" +
		    "Durham, NC 27703</a>"
	    );
    },0);
    
    setTimeout(function() {
	    markers['downtown-durham'] = createMarker(
		    35.997782, -78.902725,
		    "Downtown Durham Campus",
		    "/locations/downtown-durham",
		    "Sundays at 9 &amp; 10:45 am<br>" +
		    "<a href='http://goo.gl/maps/T6YOI' target='_blank'>309 W Morgan St<br>" +
		    "Durham, NC 27701<br>" +
		    "The Carolina Theatre</a>"
	    );
    },0);
    
    setTimeout(function() {
	    markers['north-durham'] = createMarker(
		    36.052487, -78.932664,
		    "North Durham Campus",
		    "/locations/north-durham",
		    "Sundays at 9 <i style='background-image: url(/wp-content/themes/the-summit-church-child/images/sign-language-service-dark.png); background-size: 100%; background-repeat: no-repeat; background-position-y: 3px; padding-left: 18px; margin-right: 4px;'></i> &amp; 10:45 am<br>" +
		    "<a href='http://goo.gl/maps/sQV6N' target='_blank'>3218 Rose of Sharon Rd<br>" +
		    "Durham, NC 27712<br>" +
		    "Riverside High School</a>"
	    );
    },0);
    
    setTimeout(function() {
	    markers['north-raleigh'] = createMarker(
		    35.862534, -78.590011,
		    "North Raleigh Campus",
		    "/locations/north-raleigh",
			"Saturdays at 4 &amp; 5:30 pm<br>" +
		    "Sundays at 9 &amp; 10:45 am<br>" +
		    "<a href='http://goo.gl/maps/3MJEXoCKMj12' target='_blank'>5808 Departure Dr<br>" +
		    "Raleigh, NC 27616</a>"
	    );
    },0);
    
    setTimeout(function() {
	    markers['chapel-hill'] = createMarker(
		    35.959491,-79.030444,
		    "Chapel Hill Campus",
		    "/locations/chapel-hill/",
		    "Sundays at 9 &amp; 10:45 am<br>" +
		    "<a href='http://goo.gl/maps/j3oVJ' target='_blank'>500 Weaver Dairy Rd<br>" +
		    "Chapel Hill, NC 27514<br>" +
		    "East Chapel Hill High School</a>"
	    );
    },0);
    
    if (hash && hash!="") {
	    setTimeout(function() {
			var m=markers[hash];
			if(!m)return;
			openMarker=m.window;
			m.window.open(map,m.marker);		
			console.log(m,m.window,m.marker,map);
	    },2000);
    }
}

function onPositionFail(error){}

function onPositionSuccess(position) {
	var coords = position.coords || position.coordinate || position;
	var latLng = new google.maps.LatLng(coords.latitude, coords.longitude);
	var marker = new google.maps.Marker({
		position: latLng,
		animation: google.maps.Animation.DROP,
		icon: '/wp-content/themes/the-summit-church-child/images/you-are-here.png',
		title: 'You are Here',
		map: map
	});
}