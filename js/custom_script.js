/*
	- Window Load
		- Fullscreen Menu
		- Sermon Pages subnav/class
		- Stories Page class
		- Twitter Widget Custom CSS
		- HOMEPAGE UPDATE
		- Content Bottom Announcment Bar
	- Window Resize
	- Window Scroll
	- Minimize Header/Menu
	- Navigation
	- Sub Navigation Mobile Check
	- Button Left-Aligned White-Space Fix
	- Document Ready
		- initSideMenu()
		- Header
		- Fullscreen Section
		- Fullscreen Menu
		- Fullscreen Search Menu
		- Interior Pages
		- Messages Single Page
		- Grid Loader
			- Students Events Grid
		- Homepage - Summit Story
		- Campus Pages
*/

var $j = jQuery.noConflict();
var isMobile;

/* Window Load
============================================ */

$j(window).load(function() {
	
	//console.log('window load');

	if ($j(window).width() <= 1200) {
		isMobile = true;
	} else {
		isMobile = false;
	}
	
	// Fullscreen Menu
	
	// Override the existing click function in default.js to stop animation
	$j('a.popup_menu').off('click');
	$j('a.popup_menu').on('click',function(e) {
	    e.preventDefault();

		//console.log('popup menu button clicked');

		if (!$j(this).hasClass('opened')) {

            $j(this).addClass('opened');
            $j('body').addClass('popup_menu_opened');
            $j('.side_menu_button>a.popup_menu .menu-title').fadeIn('fast');

            setTimeout(function(){
				if(!$j('body').hasClass('page-template-full_screen-php')){
					$j('body').css('overflow','hidden');
				}
            },400);

        } else {

            $j(this).removeClass('opened');
            $j('body').removeClass('popup_menu_opened');

            setTimeout(function(){
				if(!$j('body').hasClass('page-template-full_screen-php')){
					$j('body').css('overflow','visible');
				}
				/*
                $j("nav.popup_menu ul.sub_menu").slideUp(200, function(){
                    $j('nav.popup_menu').getNiceScroll().resize();
                });
				*/
            },400);

        }

	    initNav();

	});

	// Change #message link in main nav if on interior page
	if ( (window.location.pathname != '' && window.location.pathname != '/') || window.location.search != '') {
		$j('#nav-menu-item-80').find('a span:not(.plus)').text('Messages');
		$j('#nav-menu-item-80').find('a').attr('href','/messages/');
		buttonWidthFix();
	} else {
		/*if (isMobile == false) {
			$j('#nav-menu-item-80').find('a span:not(.plus)').text('Latest Message');
		}
		if (document.getElementById("latest-message")) {
			$j('#nav-menu-item-80 a').attr('href', document.getElementById("latest-message").value); // Messages send to latest series message page
		}*/
		$j('#nav-menu-item-80 a').attr('href', "/messages/");
		$j('.carousel-inner .slider_content .text .qbutton.white').each(function( index ) {
			if ($j(this).attr('href') == "/news/") {
				$j(this).attr('target', '_blank');
			}
		});
	    buttonWidthFix();
    }

    // Current Series subnav on sermon pages
    if (document.getElementById("current-series")) {
	    $j('#current_series_button a').attr('href', "/series/" + document.getElementById("current-series").value);

	    // add active state if series in view is the current series
	    if (document.getElementById("this-series") && document.getElementById("this-series").value == document.getElementById("current-series").value) {
		    $j('#current_series_button').addClass('current_page_item');
	    }

    }

    // Latest Message subnav on sermon pages
    if (document.getElementById("latest-message")) {
	    $j('#latest_message_button a').attr('href', document.getElementById("latest-message").value);
	    
	    // add active state if message in view is the latest message
	    if (document.getElementById("this-message") && document.getElementById("this-message").value == document.getElementById("latest-message-name").value) {
		    $j('#latest_message_button').addClass('current_page_item');
		    $j('#current_series_button').removeClass('current_page_item');
	    }
    }
	
	// Add .messages class to /messages page
	if (window.location.pathname == '/messages' || window.location.pathname == '/messages/') {
		$j('.full_width_inner').addClass('messages');
	} else {
		if ( !$j('.title_holder .subtitle')[0] ) { $j('.title .separator').remove() }
	}
	
	// Drop Down Sorting on /messages pages
	if (document.location.pathname.indexOf('/message/') == 0 ||
		document.location.pathname.indexOf('/messages/') == 0 ||
		document.location.pathname.indexOf('/messages') == 0 ||
		document.location.pathname.indexOf('/preacher') == 0 ||
		document.location.pathname.indexOf('/book') == 0 ||
		document.location.pathname.indexOf('/series/') == 0) {
		$j( function() {

			$j( '#wpfc_sermon_series' ).dropdown();
			$j( '#wpfc_preacher' ).dropdown();
			$j( '#wpfc_bible_book' ).dropdown();

		});
	}
	
	// Add .stories class to /stories page
	if (window.location.pathname == '/stories' || window.location.pathname == '/stories/') {
		$j('.container_inner').addClass('stories');
	}
	

	/* Twitter Widget Custom CSS
	============================================ */
	twitterCheck = setInterval(function() {
		var twitterFrame = $j('#twitter-widget-0');
        var twitterTimeline = twitterFrame.contents().find('.timeline-Widget');
        if(twitterFrame.length && twitterTimeline.length) {
            twitterTimeline.attr('style','max-width:100% !important;');
            twitterFrame.attr('style','max-width:100% !important; width: 100% !important;');
            twitterFrame.contents().find('.timeline-Tweet-brand').remove();
            twitterFrame.contents().find('.timeline-Tweet-author').remove();
            twitterFrame.contents().find('.timeline-Tweet').attr('style','background-color:transparent !important;');
            twitterFrame.contents().find('.timeline-Tweet-media').remove();
            twitterFrame.contents().find('.timeline-Tweet-actions').remove();
            twitterFrame.contents().find('.timeline-Tweet-metadata').remove();
            twitterFrame.contents().find('.timeline-Tweet-text').attr('style','font-family: "open_sans_regular", sans-serif !important; font-style: normal !important; font-weight: lighter !important; color: #454545 !important; text-align: center !important; margin-left: 0 !important;');
            twitterFrame.contents().find('.timeline-Tweet-text a').attr('style','color:#0cc1ea !important;');
            clearInterval(twitterCheck);
        }
    }, 10);
    
    
    /* HOMEPAGE UPDATE
	============================================ */
	
	if ( window.location.pathname == '/' && window.location.search.substring(0,3) != '?s=' ) {
		
		// Slider buttons
		$j(".carousel-inner .slider_content .text").prepend('<h6>Latest Message</h6><div class="separator small" style="height: 2px; width: 22px !important; background-color: #ffffff !important; margin: 10px auto 20px auto !important;"></div></div>');
		$j(".carousel-inner h2.q_slide_title span").text(document.getElementById("latest-message-title").value);
		$j(".carousel-inner p.q_slide_text span").text(document.getElementById("latest-message-scripture").value);
		$j(".carousel-inner .slider_content .text .qbutton:contains('View Message')").attr('href', document.getElementById("latest-message").value);
		$j(".carousel-inner .slider_content .text .qbutton:contains('News & Events')").text("Watch Full Service").attr('href', "/watch/");
		
		// Latest Message
		var vimeoVideoID = document.getElementById("latest-message-video").value;
		var vimeoThumbPoster;
		$j.getJSON('https://www.vimeo.com/api/v2/video/' + vimeoVideoID + '.json?callback=?', {format: "json"}, function(data) {
			var vimeoThumbLg = data[0].thumbnail_large;
			var vimeoThumbPoster = vimeoThumbLg.substr( 0, vimeoThumbLg.lastIndexOf('_') ) + '.jpg';
			$j('.home.homepage-update #latest-message-section').css('background-image', 'url("' + vimeoThumbPoster + '")');
			$j('.home.homepage-update #latest-message-button').attr('href', document.getElementById("latest-message").value);
		});
		
		$j('.home.homepage-update #latest-message-section .full_screen_section_overlay').remove();
		
	}   
    
    
	/* Content Bottom Announcment Bar
	============================================ */
	if ( $j('.content_bottom').length ) {
	    
	    $j('.content_bottom').css('display','block');
	    
		$j('.content_bottom .header_bottom_center_widget a.close').on('tap click', function(e) {
            
            e.preventDefault();
            $j('.content_bottom').addClass('hidden');
            
            return false;
            
        });

    }

});


/* Window Resize
============================================ */

function windowSize() {
    //isMobile = $j(window).width() < 1000; // BOOLEAN
    if ($j(window).width() <= 1200) {
		isMobile = true;
	} else {
		isMobile = false;
	}
    initNav();
    subnavCheck();

    $j('body').animate({
		opacity: 1
	}, 'fast', function() {
		// Animation complete.
	});

}
$j(window).on("load resize", windowSize);


/* Window Scroll
============================================ */

var windowTop = $j(window).scrollTop();
var scrollDirection;

$j(window).scroll(function() {
	minimizeHeader();
});


/* Minimize Header/Menu
============================================ */

function minimizeHeader() {

	//console.log('min header');

	var curTop = $j(window).scrollTop();

	if ( $j('header.page_header').hasClass('fixed') && !$j('body').hasClass('popup_menu_opened') ) {

		// Minimize Logo Scroll
		if ( curTop <= 0 ) {

			//console.log('scrolled up, show');

			// Scrolled Up (Show the Top Nav)
			$j('nav').removeClass('scrolled');
			$j('nav').fadeIn('fast');
			if (isMobile == false) { $j('.logo_wrapper').removeClass('scrolled'); }
			$j('.side_menu_button>a.popup_menu').removeClass('scrolled');
			$j('.side_menu_button>a.search_button').removeClass('scrolled').fadeIn('fast');
			$j('.side_menu_button>a.popup_menu .menu-title').fadeIn('fast');
			$j('nav.main_menu.right').removeClass('fullscreen');
	        $j('.side_menu_button>a.popup_menu').removeClass('fullscreen');
			$j('.side_menu_button>a.search_button').removeClass('fullscreen');

		} else {

			//console.log('scrolled down, hide');

			// Scrolled Down (Hide the Top Nav)
			$j('nav').addClass('scrolled');
			$j('nav').fadeOut('fast');
			if (isMobile == false) { $j('.logo_wrapper').addClass('scrolled'); }
			$j('.side_menu_button>a.search_button').addClass('scrolled').fadeOut('fast');
			$j('.side_menu_button>a.popup_menu .menu-title').fadeOut('fast', function() {
				$j('.side_menu_button>a.popup_menu').addClass('scrolled');
			});

		}

		windowTop = curTop;

	} else {

		//console.log('popup open');

		// hide the footer nav if the window height shrinks
		var footerNavTop = $j('.popup_menu_footer_container').offset().top;
		var navTop = ( $j('nav.popup_menu').offset().top + $j('nav.popup_menu').height() );
		if (footerNavTop <= navTop) {
			$j('.popup_menu_footer').css('opacity','0');
		} else {
			$j('.popup_menu_footer').css('opacity','1');
		}

		// Show the Top Nav regardless of scroll position
		$j('nav').removeClass('scrolled');
		$j('nav').fadeIn('fast');
		if (isMobile == false) { $j('.logo_wrapper').removeClass('scrolled'); }
		$j('.side_menu_button>a.popup_menu').removeClass('scrolled');
		$j('.side_menu_button>a.search_button').removeClass('scrolled').fadeIn('fast');

	}

}


/* Navigation
============================================ */

var currentNavSection;
var currentNavSectionID;

function initNav() {

	//console.log('init nav');

	$j('#popup-menu-item-2455 a').attr('target', '_blank'); // Sam James Institute

	if ( $j('subnav .subnav_container').hasClass('parent_included') && !$j('subnav .subnav_container ul li').hasClass('current_page_item') ) {
		$j('subnav .subnav_container ul li:first-child').addClass('current_page_item');
	}

	if (isMobile == false) {

		//console.log('initnav - mobile false');

		$j('header').addClass('fixed');
		$j('.header_inner_right').css('position','fixed');
		$j('.header_inner_right').css('right','25px');

		$j('nav.popup_menu ul li ul.sub_menu').css('display', 'inline-block');

		// Disable the click of the sub menu titles
		$j(".popup_menu > ul > li.has_sub > a, .popup_menu > ul > li.has_sub > h6").off('tap click');

		minimizeHeader();

	} else {

		//console.log('initnav - mobile true');

		if ($j('body').hasClass('popup_menu_opened')) {
			$j('header').addClass('fixed');
			$j('.header_inner_right').css('position','fixed');
			//$j('nav.main_menu.right').addClass('open');
		} else {
			$j('.header_inner_right').css('position','absolute');
			$j('nav.main_menu.right').css('display','none !important');
			//$j('nav.main_menu.right').removeClass('open');
		}

		$j('nav.popup_menu ul li ul.sub_menu').css('display', 'none');

		//logic for reveal sub menus in popup menu
		$j(".popup_menu > ul > li.has_sub > a, .popup_menu > ul > li.has_sub > h6").off('tap click');
        $j(".popup_menu > ul > li.has_sub > a, .popup_menu > ul > li.has_sub > h6").on('tap click', function(e) {
            e.preventDefault();

            if ($j(".popup_menu > ul > li.has_sub").find("> ul.sub_menu").is(":visible")){
                $j(".popup_menu > ul > li.has_sub").find("> ul.sub_menu:visible").slideUp('fast', function() {
                    $j('.popup_menu_holder_outer').getNiceScroll().resize();
                });
                $j(".popup_menu > ul > li.has_sub").find("> ul.sub_menu:visible").parent().removeClass('open_sub');
            }

            if ($j(this).closest('li.has_sub').find("> ul.sub_menu").is(":visible")){
                $j(this).closest('li.has_sub').find("> ul.sub_menu").slideUp('fast', function() {
                    $j('.popup_menu_holder_outer').getNiceScroll().resize();
                });
                $j(this).closest('li.has_sub').removeClass('open_sub');
            } else {
                $j(this).closest('li.has_sub').addClass('open_sub');
                $j(this).closest('li.has_sub').find("> ul.sub_menu").slideDown('slow', function() {
                    $j('.popup_menu_holder_outer').getNiceScroll().resize();
                });
            }

            return false;
        });

        minimizeHeader();

	}

}


/* Sub Navigation Mobile Check
============================================ */

function subnavCheck() {
	if (isMobile == true) {
		$j('subnav').addClass('mobile');

		// Campus Page Events
		$j('#tc_wp_item_3').hide();
		$j('#tc_wp_item_4').hide();

	} else {
		$j('subnav').removeClass('mobile');
		$j('subnav .subnav_container').removeAttr('style');
		$j('subnav').removeClass('mobile_open');

		// Campus Page Events
		$j('#tc_wp_item_3').show();
		$j('#tc_wp_item_4').show();
	}
}


/* Button Left-Aligned White-Space Fix
============================================ */

var current_button_height;
var button_height_small = 30;
var button_height_medium = 20;
var button_height_large = 40;
var button_height_xlarge = 58;

function buttonWidthFix() {

	//console.log('buttonWidthFix');

	var button_count = $j( ".qbutton" ).length;
	var button_array = [];

	$j(".qbutton").each(function( index ) {

		//console.log(index + " : " + $j(this).width());
		//$j(this).attr('id', 'button' + index).attr('data-width', $j(this).width());
		//button_array[index] = [$j(this).attr('id'),$j(this).width()];

		if ( $j(this).hasClass('big_large') ) {
	        current_button_height = button_height_xlarge;
	    } else if ( $j(this).hasClass('large') ) {
	        current_button_height = button_height_large;
	    } else if ( $j(this).hasClass('medium') ) {
	        current_button_height = button_height_medium;
	    } else if ( $j(this).hasClass('small') ) {
	        current_button_height = button_height_small;
	    } else {
	        // default is 'medium'
	        current_button_height = button_height_medium;
	    }

	    $j(this).css('white-space', 'nowrap');

	    if (isMobile == false) {

		    if ( $j(this).width() >= $j(this).closest('.wpb_column').width() ) {
			    $j(this).css('max-width', $j(this).closest('.wpb_column').width()+'px');
				$j(this).css('white-space', 'normal');
			    var html_org = $j(this).html();
			    var html_calc = '<span>' + html_org + '</span>';
			    $j(this).html(html_calc);
			    var width = $j(this).find('span:first').width();
			    $j(this).html(html_org);
			    $j(this).width(width);
				$j(this).css('white-space', 'normal');
			}

		} else {

			if ( $j(this).width() >= $j(this).closest('.wpb_column').width() ) {
			    $j(this).css('max-width', $j(this).closest('.wpb_column').width()+'px');
				$j(this).css('white-space', 'normal');
			    var html_org = $j(this).html();
			    var html_calc = '<span>' + html_org + '</span>';
			    $j(this).html(html_calc);
			    var width = $j(this).find('span:first').width();
			    $j(this).html(html_org);
			    $j(this).width(width);
				$j(this).css('white-space', 'normal');
			}

		}

	});

/*
	$j.each(button_array, function(index, value){
		console.log("INDEX: " + index + " VALUE: " + value);
	});
*/

}


/* Document Ready
============================================ */

$j(document).ready(function() {
	
	// HOMEPAGE UPDATE (also in window.load)
	//if ( (window.location.pathname == '/homepage-update') || window.location.pathname == '/homepage-update/') {
	if ( window.location.pathname == '/' && window.location.search.substring(0,3) != '?s=' ) {
		$j('body').addClass('homepage-update');
	}
	if ( window.location.pathname == '/homepage-2017test/' ) {
		$j('body').addClass('home').addClass('homepage-update');
	}
	// HOMEPAGE UPDATE - END

	//console.log('doc ready');

	// Facebook Custom Open Graph Meta (og:image)
	if ($j('body').hasClass('home')) {
		$j('head').append('<meta property="og:url" content="'+window.location.href+'" />\
		<meta property="og:title" content="'+document.title+'" />\
		<meta property="og:description" content="Love God. Love Each Other. Love Our World." />\
	    <meta property="og:image" content="<?php echo get_stylesheet_directory_uri(); ?>/images/facebook-sharer-2015.jpg">')
/*
	} else if ($j('body').hasClass('single-wpfc_sermon')) {
	    var header_bg = $j('.attachment-sermon-wide').attr('src');
	    $j('head').append('<meta property="og:url" content="'+window.location.href+'" />\
		<meta property="og:title" content="'+document.title+'" />\
		<meta property="og:description" content="'+$j('.subtitle').text()+'" />\
		<meta property="og:image" content='+header_bg+' />\
		<meta name="description" content="'+$j('.subtitle').text()+'" />');
*/
	} else {
	    var header_bg = $j('.header_bg').css('background-image');
	    header_bg = header_bg.replace('url(','').replace(')','');
	    $j('head').append('<meta property="og:url" content="'+window.location.href+'" />\
		<meta property="og:title" content="'+document.title+'" />\
		<meta property="og:description" content="'+$j('.subtitle').text()+'" />\
		<meta property="og:image" content='+header_bg+' />\
		<meta name="description" content="'+$j('.subtitle').text()+'" />');
	}

	// Load random carousel slider image
	if ($j('body').hasClass('home')) {
		$j('.carousel-indicators').remove();
		$j('.carousel-control').remove();

		var carouselImages = new Array();
		var numImages = $j('.carousel-inner').find('.item').length;
	    var randomNum = Math.floor( Math.random() * numImages );
		$j('.carousel-inner .item').each(function(index, elem) {
		    $j(this).addClass('active');
		    carouselImages.push( $j(this) );
		    //if ( index != randomNum ) { $j(this).remove() }
		    if ( index != 0 ) { $j(this).remove() }
		});

		// live feed error message safety net
		if ( $j('.hungryfeed_item.error')[0] ) {
			if ( $j(this).find('.jd-greear-feed') ) {
				$j('.jd-greear-feed a.qbutton').attr('href','http://www.jdgreear.com/');
			}
			if ( $j(this).find('.sji-feed') ) {
				$j('.sji-feed a.qbutton').attr('href','http://sji.summitrdu.com/');
			}
		}

	}

	/*	Opening side menu on "menu button" click
		Also commented out initSideMenu() in js/default.min.js
	============================================================= */

	var current_scroll;
	function initSideMenu() {
		"use strict";

		$j('.side_menu_button_link').remove();

		if ($j('body').hasClass('side_menu_slide_from_right')) {
			$j('.wrapper').prepend('<div class="cover"/>');
			$j('#nav-menu-item-18, a.side_menu_close').click(function(e) {
				e.preventDefault();

				if (!$j('#nav-menu-item-18').hasClass('opened')) {
					$j(this).addClass('opened');
					$j('body').addClass('right_side_menu_opened');

					current_scroll = $j(window).scrollTop();
					$j(window).scroll(function() {
						if(Math.abs($scroll - current_scroll) > 400){
							$j('body').removeClass('right_side_menu_opened');
							$j('.side_menu_button_wrapper a').removeClass('opened');
						}
					});

				} else {
					$j('#nav-menu-item-18').removeClass('opened');
					$j('body').removeClass('right_side_menu_opened');
				}
			});
		}
	}


	/* Header
	============================================ */

	// RefTagger Plugin (Bible Verses / customize at http://reftagger.com/customize/)
	//$j('body').append('<script>var refTagger = { settings: { bibleVersion: "ESV", noSearchClassNames: ["tc_wp_item"], socialSharing: [], tooltipStyle: "light", tagChapters: true } };</script>');

	$j('body').append('<script>var refTagger = { settings: { bibleVersion: "ESV", noSearchClassNames: ["tc_wp_item","blockquote-text","cd-dropdown","title_holder","bible_wrapper"], socialSharing: [], tooltipStyle: "light", tagChapters: true } }; (function(d, t) { var g = d.createElement(t), s = document.getElementsByTagName("body")[0]; g.src = "/wp-content/plugins/reftagger/reftagger.js"; document.body.appendChild(g); }(document, "script"));</script>');

	// Add Title To Menu Button
	$j('.side_menu_button>a.popup_menu').prepend('<span class="menu-title">Menu</span>');

	$j(".popup_menu > ul > li.has_sub > a, .popup_menu > ul > li.has_sub > h6").append('<div class="separator small center" style="height: 2px;"></div>');


	/* Fullscreen Section
	============================================ */

	$j('.full_screen_section').append('<div class="full_screen_section_overlay"></div>');


	/* Fullscreen Menu
	============================================ */

	// Hide the repeated top navigation items. The already existing items' styles will be changed with css.
	$j('nav.popup_menu ul li').each(function() {
		if (!$j(this).hasClass('has_sub') && !$j(this).parent().hasClass('sub_menu')) {
			$j(this).css('display', 'none');
		}
	});

	// Show top navigation in fullscreen menu
/*
	$j('a.popup_menu').on('click',function(e) {
	    e.preventDefault();
		//initNav();

	});
*/

	/* Fullscreen Search Menu
	============================================ */

	$j('.fullscreen_search_holder').addClass('hidden');
	$j('a.search_button').on('click',function(e) {
		e.preventDefault();
		if (isMobile == false) {
			$j('.fullscreen_search_holder .search_field').attr('placeholder','type to search');
		} else {
			$j('.fullscreen_search_holder .search_field').attr('placeholder','search');
		}
		$j('.fullscreen_search_holder.fade').removeClass('hidden');
		$j('body').css('position','fixed');
		setTimeout(function() { $j('.fullscreen_search_holder .search_field').focus() }, 300);
	});

	$j('a.fullscreen_search_close').on('click',function(e) {
		$j('.fullscreen_search_holder').addClass('hidden');
		$j('body').css('position','initial');
	});


	/* Interior Pages Columns
	============================================ */

	$j('.vc_col-sm-6').addClass('vc_col-xs-12');


	/* Interior Pages Subnav
	============================================ */

	$j('subnav .mobile_subnav_holder a').on('click',function(e) {
		e.preventDefault();

		if ($j('subnav').hasClass('mobile_open')) {
			$j('subnav.mobile_open .subnav_container').slideUp('fast', function() {
				$j('subnav').removeClass('mobile_open');
			});
		} else {
			$j('subnav .subnav_container').slideDown('fast');
			$j('subnav').addClass('mobile_open');
		}

	});


	/* Interior Pages Tertiary Subnav
	============================================ */

	$j('subnav .page_item_has_children').mouseenter(function(e) {
	    $j(this).children('ul.children').show();
	});

	$j('subnav .page_item_has_children').mouseleave(function(e) {
	    $j(this).children('ul.children').hide();
	});
/*
	$j('subnav .page_item_has_children a').hover(
		function() {
			$j(this).next('ul.children').addClass('open');
		}, function() {
			$j(this).next('ul.children').removeClass('open');
		}
	);
*/

/*
		if ($j('subnav').hasClass('mobile_open')) {
			$j('subnav.mobile_open .subnav_container').slideUp('fast', function() {
				$j('subnav').removeClass('mobile_open');
			});
		} else {
			$j('subnav .subnav_container').slideDown('fast');
			$j('subnav').addClass('mobile_open');
		}
*/


	/* Messages Single Page
	============================================ */

	if (document.getElementById('wpfc_sermon-video') !== null) {

		$j.getScript( 'http://a.vimeocdn.com/js/froogaloop2.min.js' )
			.done(function( script, textStatus ) {
				var iframe = $j('.wpfc_sermon-video iframe')[0];
		        var player = $f(iframe);

		        $j('.wpfc_sermon_player .wpfc_sermon-video-buttons .qbutton.video').click(function() {
					$j('audio').each(function() {
						$j(this)[0].pause();
					});
				});

		        $j('.wpfc_sermon_player .wpfc_sermon-video-buttons .qbutton.audio').click(function() {
					player.api('pause');
				});


			})
			.fail(function( jqxhr, settings, exception ) {
			    // script failed to load
		});

		$j('.wpfc_sermon_player .wpfc_sermon-video-buttons .qbutton').click(function(e) {
			if ($j(this).hasClass('on')) {
			    e.preventDefault();
			} else {
				if ($j(this).attr('href') == 'video') {
					$j(this).addClass('on');
					$j('.wpfc_sermon_player .wpfc_sermon-video-buttons .qbutton.audio').removeClass('on');
					$j('.wpfc_sermon-audio').hide();
					$j('.wpfc_sermon-video').show();
					$j('.wpfc_sermon-audio').removeClass('on');
				} else if ($j(this).attr('href') == 'audio') {
					$j(this).addClass('on');
					$j('.wpfc_sermon_player .wpfc_sermon-video-buttons .qbutton.video').removeClass('on');
					$j('.wpfc_sermon-video').hide();
					$j('.wpfc_sermon-audio').show();
					$j('.wpfc_sermon-audio').addClass('on');
				}
				return false;
			}
		});

	}


	/* Grid Loader
	============================================ */

	if ($j('.vc_grid-loading') && $j('.vc_grid-container').hasClass('the-city') || $j('.vc_grid-loading') && $j('.vc_grid-container').hasClass('students-events')) {

		$j('.vc_grid-container').append('<div class="vc_grid-loading-bg"></div>');

		var vc_grid_visible;

		function checkGridVisible() {
			vc_grid_visible = setInterval(gridVisible, 100);
		}

		function gridVisible() {
			if($j('.vc_grid').is(':visible')){
	            $j('.vc_grid-loading-bg').remove();
	            stopGridCheck();    
    
			    /* Students Events Grid
				============================================ */
			    $j('.vc_basic_grid.students-events .vc_grid.vc_row .vc_grid-item.vc_visible-item').each(function() {
					$j(this).find('a img').unwrap();
			    });
	            
	        }
		}

		function stopGridCheck() {
			clearInterval(vc_grid_visible);
		}

		checkGridVisible();

	}


	/* Homepage - Summit Story
	============================================ */

	/* copy/paste this into the buttons text block in the Grid Element 'Summit Story'
<a id="morestories" class="qbutton white" style="white-space: nowrap;" href="/stories" target="_self">More Stories</a>
<script type="text/javascript">
var fullstory_link = $j('.vc_gitem-link').attr('href');
var fullstory_html = '<a class="alignleft qbutton white fullstory marginright" style="white-space: nowrap;" target="_self" href="' + fullstory_link + '">Read The Full Story</a>';
$j('#morestories').parent().prepend(fullstory_html);
</script>
	*/

	/* Campus Pages
	============================================ */

	if (document.getElementById('campus-header') !== null) {
		$j('#campus-header').clone().prop('id', 'campus-header-clone' ).appendTo('.title_subtitle_holder_inner');
		$j('#campus-header').hide();

		// fix Campus Event Title truncation
		//$j('.campus-events .the_city_plaza_widget .tc_wp_item').dotdotdot();
	}

});
