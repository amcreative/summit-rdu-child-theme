import cssnano from 'gulp-cssnano';
import gulp from 'gulp';
import livereload from 'gulp-livereload';
import pump from 'pump';
import rename from 'gulp-rename';
import sourcemaps from 'gulp-sourcemaps';

/**
 * Function to concat all files in a src directory.
 *
 * @author  Allen Moore
 * @param   {Object}   atts an Object of file properties.
 * @param   {Function} cb   the pipe sequence that gulp should run.
 * @returns {void}
 */
gulp.task('cssnano', () => {
  return gulp.src('./style.css')
    .pipe(sourcemaps.init({loadMaps: true}))
    .pipe(cssnano({
      autoprefixer: false,
      calc: {precision: 8},
      convertValues: true
    }))
    .pipe(rename((path) => {
      path.extname = '.min.css'
    }))
    .pipe(sourcemaps.write('./'))
    .pipe(gulp.dest('./'))
    .pipe(livereload());
});
