import babelify from 'babelify';
import browserify from 'browserify';
import buffer from 'vinyl-buffer';
import gulp from 'gulp';
import livereload from 'gulp-livereload';
import log from 'gulplog';
import sourcemaps from 'gulp-sourcemaps';
import tap from 'gulp-tap';

/**
 * Function to concat all files in a src directory.
 *
 * @author  Allen Moore
 * @param   {Object}   atts an Object of file properties.
 * @param   {Function} cb   the pipe sequence that gulp should run.
 * @returns {void}
 */
gulp.task('javascript', () => {
  const opts = {
    dest: './src/js/compiled',
    src: './src/js/components/**/*.js'
  };

  return gulp.src(opts.src, {read: false})
    .pipe(tap((file) => {
      log.info('bundling' + file.path);
      file.contents = browserify(file.path, {debug: true}).transform(babelify).bundle();
    }))
    .pipe(buffer())
    .pipe(sourcemaps.init({loadMaps:true}))
    .pipe(sourcemaps.write('./'))
    .pipe(gulp.dest(opts.dest))
    .pipe(livereload());
});

