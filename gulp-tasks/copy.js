import gulp from 'gulp';
import livereload from 'gulp-livereload';

/**
 * Function to concat all files in a src directory.
 *
 * @author  Allen Moore
 * @param   {Object}   atts an Object of file properties.
 * @param   {Function} cb   the pipe sequence that gulp should run.
 * @returns {void}
 */
gulp.task('copy', () => {
  return gulp.src(['./src/js/compiled/*.js', './src/js/compiled/*.js.map'])
    .pipe(gulp.dest('./dist/js'))
    .pipe(livereload());
});
