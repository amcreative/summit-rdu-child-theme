<?php

class NextSundayDate {

	/**
	 * The NextSundayDate constructor.
	 */
	public function __construct() {
		add_action( 'wp_ajax_summit_watch_live', array( $this, 'watchLiveCTA' ) );
		add_action( 'wp_ajax_nopriv_summit_watch_live', array( $this, 'watchLiveCTA' ) );
		add_action( 'wp_enqueue_scripts', array( $this, 'enqueueScripts' ) );
	}

	/**
	 * Method to return date of upcoming Sunday.
	 *
	 * @author Allen Moore
	 * @return string
	 */
	public function getDate() {

		$date = new DateTime();
		$date->modify( 'next sunday' );

		return $date->format( 'm/d/Y' );
	}

	/**
	 * Method to enqueue the JavaScript.
	 *
	 * @author Allen Moore
	 * @return void
	 */
	public function enqueueScripts() {

		$baseUrl = trailingslashit( get_stylesheet_directory_uri() );
		$min = defined( 'SCRIPT_DEBUG' ) && filter_var( SCRIPT_DEBUG, FILTER_VALIDATE_BOOLEAN ) ? '' : '.min';

		if ( ! is_front_page() ) {
			return;
		}

		wp_enqueue_script(
			'watch-live',
			"{$baseUrl}dist/js/watch-live-button{$min}.js",
			array(),
			SUMMIT_RDU_VERSION,
			true
		);

		$date = $this->getDate();
		$data = array(
			'date'  => $date,
		);

		wp_localize_script(
			'watch-live',
			'watchLive',
			$data
		);
	}
}

$nextSundayDate = new NextSundayDate();
