<?php

class BlockRobots {

	/**
	 * The BlockRobots constructor.
	 */
	public function __construct() {
		add_action( 'wp_head', [$this, 'addMetaTag'] );
	}

	/**
	 * Method to add a `<meta` tag to the header to block search engine indexing.
	 *
	 * @author Allen Moore
	 * @return void
	 */
	public function addMetaTag() {

		$postTypes = [
			'portfolio_page',
			'cpt_staff_lst_item',
		];

		if ( ! is_singular( $postTypes ) ) {
			return;
		}
		?>
		<meta name="robots" content="noindex, nofollow">
		<?php
	}
}

$tscBlockRobots = new BlockRobots();