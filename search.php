<?php get_header(); ?>
<?php 
global $wp_query;
$id = $wp_query->get_queried_object_id();

if ( get_query_var('paged') ) { $paged = get_query_var('paged'); }
elseif ( get_query_var('page') ) { $paged = get_query_var('page'); }
else { $paged = 1; }

$sidebar = $qode_options_proya['category_blog_sidebar'];


if(isset($qode_options_proya['blog_page_range']) && $qode_options_proya['blog_page_range'] != ""){
	$blog_page_range = $qode_options_proya['blog_page_range'];
} else{
	$blog_page_range = $wp_query->max_num_pages;
}

?>
	
	<?php if(get_post_meta($id, "qode_page_scroll_amount_for_sticky", true)) { ?>
		<script>
			var page_scroll_amount_for_sticky = <?php echo get_post_meta($id, "qode_page_scroll_amount_for_sticky", true); ?>;
		</script>
	<?php } ?>

	<?php get_template_part( 'title' ); ?>
	
	<div class="container">
    <?php if(isset($qode_options_proya['overlapping_content']) && $qode_options_proya['overlapping_content'] == 'yes') {?>
        <div class="overlapping_content"><div class="overlapping_content_inner">
    <?php } ?>
	<div class="container_inner default_template_holder clearfix">
		<?php if($sidebar == "1" || $sidebar == "2"): ?>
		
			<div class="blog_holder blog_large_image">
				
				<div class="search_results_query">
					<div class="wpb_column vc_column_container vc_col-md-6 vc_col-sm-12">
						<div class="wpb_wrapper">
							<div class="wpb_text_column wpb_content_element">
								<div class="wpb_wrapper">
									<div class="results_query_return">                        
										<h4><?php echo $wp_query->found_posts ?> Results for "<em><?php the_search_query() ?></em>" were found.</h4>
									</div>
								</div> 
							</div>
						</div>
					</div>
					<div class="wpb_column vc_column_container vc_col-md-6 vc_col-sm-12">
						<div class="wpb_wrapper">
							<div class="wpb_text_column wpb_content_element">
								<div class="wpb_wrapper">
									<form role="search" action="https://www.summitrdu.com/" class="fullscreen_search_form inline_search" method="get">
										<div class="form_holder">
											<p class="search_label">Search Again:</p>
											<div class="field_holder">
												<input type="text" name="s" class="search_field" autocomplete="off">
												<div class="line" style="width: 0px;"></div>
											</div>
					                        <a class="qode_search_submit search_submit" href="javascript:void(0)">
					                            <i class="qode_icon_font_awesome fa fa-search "></i>
					                        </a>
										</div>	
									</form>
								</div>
							</div>
						</div>
					</div>
				</div>

				<?php if(have_posts()) : while ( have_posts() ) : the_post(); ?>
						<?php 
							get_template_part('templates/blog_search', 'loop');
						?>
				
			
				<?php endwhile; ?>
				<?php if($qode_options_proya['pagination'] != "0") : ?>
					<?php pagination($wp_query->max_num_pages, $blog_page_range, $paged); ?>
				<?php endif; ?>
				<?php else: //If no posts are present ?>
<!--
						<div class="entry">
							<p>Please try your search again.</p>
							<p><?php _e('Sorry, no results for '. the_search_query() .' were found.', 'qode'); ?></p>
						</div>
-->
						
						<article class="page type-page status-publish hentry">
							<div class="post_content_holder">
								<div class="post_text">
									<div class="post_text_inner">
										<p>Please try your search again.</p>
									</div>
								</div>
							</div>
						</article>
				<?php endif; ?>
			</div>
		<?php endif; ?>
	</div>
    <?php if(isset($qode_options_proya['overlapping_content']) && $qode_options_proya['overlapping_content'] == 'yes') {?>
        </div></div>
    <?php } ?>
</div>
	
<?php get_footer(); ?>