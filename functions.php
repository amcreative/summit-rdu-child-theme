<?php

require_once( get_stylesheet_directory() . '/includes/Modules/Privacy/BlockRobots.php' );
require_once( get_stylesheet_directory() . '/includes/Modules/JavaScriptHelpers/NextSundayDate.php' );

if ( stripos( parse_url( home_url(), PHP_URL_HOST ), 'staging' ) !== false ) {
	define( 'SUMMIT_RDU_' . 'VERSION', time() );
} else {
	define( 'SUMMIT_RDU_VERSION', '1.1.0' );
}
if ( ! defined( 'SUMMIT_RDU_URL' ) ) {
	define( 'SUMMIT_RDU_URL', get_stylesheet_directory_uri() );
}
if ( ! defined( 'SUMMIT_RDU_TEMPLATE_URL' ) ) {
	define( 'SUMMIT_RDU_TEMPLATE_URL', get_template_directory_uri() );
}
if ( ! defined( 'SUMMIT_RDU_PATH' ) ) {
	define( 'SUMMIT_RDU_PATH', trailingslashit( get_template_directory() ) );
}
if ( ! defined( 'SUMMIT_RDU_INC' ) ) {
	define( 'SUMMIT_RDU_INC', SUMMIT_RDU_PATH . 'includes/' );
}

/* Disable specific Plugin Update Notices
============================================ */

/*
function disable_plugin_updates( $value ) {
	unset( $value->response['sermon-manager-for-wordpress/sermons.php'] );
	return $value;
}
add_filter( 'site_transient_update_plugins', 'disable_plugin_updates' );
*/

/* Increased Security Measures!!!!!!!!
============================================ */

// Remove The Version Number From WordPress Header
remove_action('wp_head', 'wp_generator');

// Also remove the shoutout to Visual Composer
	// in file: /wp-content/plugins/js_composer/include/classes/core/class-vc-base.php
	// in function: public function addMetaData()
	// Comment Out:
	// echo '<meta name="generator" content="Powered by Visual Composer - drag and drop page builder for WordPress."/>' . "\n";


/* Custom CSS/JS
============================================ */

function wp_schools_enqueue_scripts() {

	$baseUrl = trailingslashit( get_stylesheet_directory_uri() );
	$min = defined( 'SCRIPT_DEBUG' ) && filter_var( SCRIPT_DEBUG, FILTER_VALIDATE_BOOLEAN ) ? '' : '.min';

	wp_register_style(
		'childstyle',
		"{$baseUrl}style{$min}.css",
		array(),
		SUMMIT_RDU_VERSION,
		'all'
	);
	wp_enqueue_style( 'childstyle' );

	//wp_enqueue_script( 'dotdotdot-script', get_stylesheet_directory_uri() . '/js/jquery.dotdotdot.min.js', array( 'jquery' ), false, false );
	//wp-content/themes/the-summit-church-child/js/jquery.dropdown.js
	wp_enqueue_script( 'custom-script', get_stylesheet_directory_uri() . '/js/custom_script.js', array( 'jquery' ), false, true );
	wp_enqueue_script( 'dropdown-script', get_stylesheet_directory_uri() . '/js/jquery.dropdown.js', array( 'jquery' ), false, true );
}
add_action( 'wp_enqueue_scripts', 'wp_schools_enqueue_scripts', 11);


/* Remove version number from CSS/JS files
============================================ */

// remove wp version param from any enqueued scripts
function vc_remove_wp_ver_css_js( $src ) {
	if ( strpos( $src, 'ver=' ) )
		$src = remove_query_arg( 'ver', $src );
	return $src;
}
add_filter( 'style_loader_src', 'vc_remove_wp_ver_css_js', 9999 );
add_filter( 'script_loader_src', 'vc_remove_wp_ver_css_js', 9999 );


/* Relevanssi apostrophe fix
============================================ */
add_filter('relevanssi_remove_punctuation', 'rlv_kill_apostrophes', 9);
function rlv_kill_apostrophes($a) {
	$a = str_replace("'", '', $a);
	$a = str_replace("’", '', $a);
	$a = str_replace("‘", '', $a);
	return $a;
}

/* Add post thumbnails
============================================ */

if ( function_exists( 'add_theme_support' ) ) {
	add_theme_support( 'post-thumbnails' );
	add_image_size( 'portfolio_masonry_wide', 1200, 900, true );

	//add post formats support
	add_theme_support('post-formats', array('gallery', 'link', 'quote', 'video', 'audio'));

	//add feedlinks support
	add_theme_support( 'automatic-feed-links' );
}

if(!function_exists('qode_get_title_text')) {
	/**
	 * Function that returns current page title text. Defines qode_title_text filter
	 * @return string current page title text
	 *
	 * @see is_tag()
	 * @see is_date()
	 * @see is_author()
	 * @see is_category()
	 * @see is_home()
	 * @see is_search()
	 * @see is_404()
	 * @see get_queried_object_id()
	 * @see qode_is_woocommerce_installed()
	 *
	 * @since 4.3
	 * @version 0.1
	 *
	 */
	function qode_get_title_text() {
		global $qode_options_proya;

		$id 	= get_queried_object_id();
		$title 	= '';

		//is current page tag archive?
		if (is_tag()) {
			//get title of current tag
			$title = single_term_title("", false)." Tag";
		}

		//is current page date archive?
		elseif (is_date()) {
			//get current date archive format
			$title = get_the_time('F Y');
		}

		//is current page author archive?
		elseif (is_author()) {
			//get current author name
			$title = __('Author:', 'qode') . " " . get_the_author();
		}

		//us current page category archive
		elseif (is_category()) {
			//get current page category title
			$title = single_cat_title('', false);
		}

		//is current page blog post page and front page? Latest posts option is set in Settings -> Reading
		elseif (is_home() && is_front_page()) {
			//get site name from options
			$title = get_option('blogname');
		}

		//is current page search page?
		elseif (is_search()) {
			//get title for search page
			$title = __('Search Results', 'qode');
		}

		//is current page 404?
		elseif (is_404()) {
			//is 404 title text set in theme options?
			if($qode_options_proya['404_title'] != "") {
				//get it from options
				$title = $qode_options_proya['404_title'];
			} else {
				//get default 404 page title
				$title = __('404 - Page not found', 'qode');
			}
		}

		//is WooCommerce installed and is shop or single product page?
		elseif(qode_is_woocommerce_installed() && (is_shop() || is_singular('product'))) {
			//get shop page id from options table
			$shop_id = get_option('woocommerce_shop_page_id');

			//get shop page and get it's title if set
			$shop = get_post($shop_id);
			if(isset($shop->post_title) && $shop->post_title !== '') {
				$title = $shop->post_title;
			}

		}

		//is WooCommerce installed and is current page product archive page?
		elseif(qode_is_woocommerce_installed() && (is_product_category() || is_product_tag())) {
			global $wp_query;

			//get current taxonomy and it's name and assign to title
			$tax 			= $wp_query->get_queried_object();
			$category_title = $tax->name;
			$title 			= $category_title;
		}

		//is current page some archive page?
		elseif (is_archive()) {
			$title = __('Archive','qode');
		}

		//current page is regular page
		else {
			$title = get_the_title($id);
		}

		$title = apply_filters('qode_title_text', $title);

		return $title;
	}
}


/* Custom Shortcodes
============================================ */

function get_attachment_url_by_slug( $slug ) {
	$args = array(
		'post_type' => 'attachment',
		'name' => sanitize_title($slug),
		'posts_per_page' => 1,
		'post_status' => 'inherit',
		'class'	=> "thumbnail"
	);
	$_header = get_posts( $args );
	$header = $_header ? array_pop($_header) : null;
	return $header ? wp_get_attachment_url($header->ID) : '';
}


// Leadership Page - Get Featured Image
function featured_image_in_post( $atts, $content = null ) {
	return '<div class="post_featured_image">' . get_the_post_thumbnail($post_id, 'medium') . '</div>';
}
add_shortcode( 'featured_image_in_post', 'featured_image_in_post' );



function improved_trim_excerpt($text) {
	global $post;
	if ( '' == $text ) {
		$text = get_the_content('');
		$text = apply_filters('the_content', $text);
		$text = str_replace('\]\]\>', ']]&gt;', $text);
		$text = preg_replace('@<script[^>]*?>.*?</script>@si', '', $text);
		$text = strip_tags($text, '<div><h3><img><p><br><strong><em><i><a><span><ul><li>');

/*
		$text = str_replace('\]\]\>', ']]&gt;', $text);
		$text = preg_replace('@<script[^>]*?>.*?</script>@si', '', $text);
		$text = strip_tags($text, '<p>');
		$excerpt_length = 80;
		$words = explode(' ', $text, $excerpt_length + 1);
		if (count($words)> $excerpt_length) {
				array_pop($words);
				array_push($words, '[...]');
				$text = implode(' ', $words);
		}
*/
	}
	return $text;
}
remove_filter('get_the_excerpt', 'wp_trim_excerpt');
add_filter('get_the_excerpt', 'improved_trim_excerpt');
remove_filter( 'the_excerpt', 'wp_trim_excerpt' );
add_filter('the_excerpt', 'improved_trim_excerpt');



// Add link to Flywheel caching options to WP toolbar
function summitrdu_flush_cache_link($wp_admin_bar) {
	$args = array(
		'id' => 'summitrducache',
		'title' => 'Flush Cache',
		'href' => 'https://app.getflywheel.com/org/the-summit-church/summit-rdu/advanced',
		'meta' => array(
			'class' => 'summitrducache',
			'title' => 'Flush Cache',
			'target' => '_BLANK'
			)
	);
	$wp_admin_bar->add_node($args);
}
add_action('admin_bar_menu', 'summitrdu_flush_cache_link', 999);
