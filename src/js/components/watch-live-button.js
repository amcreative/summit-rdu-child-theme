let watchLiveBtn = {};

watchLiveBtn = {
  date: window.watchLive.date,
  delay: 60000,
  endTime: '11:50:00 AM',
  endBuffer: '11:51:00 AM',
  onAir: false,
  onAirClass: '-onair',
  startTime: '10:44:00 AM',

  /**
   * Method to change the button text.
   *
   * @author  Allen Moore
   * @param   object elem the button object.
   * @returns {void}
   */
  changeBtnText(elem) {
    if (false === this.onAir) {
      elem.innerText = 'Join Us Live Now';
    }
  },

  /**
   * Method to reset the button text.
   *
   * @author  Allen Moore
   * @param   object elem the button object.
   * @returns {void}
   */
  resetBtnText(elem) {
    if (true === this.onAir) {
      elem.innerText = 'Watch Full Service';
    }
  },

  /**
   * Method to change the button style properties.
   *
   * @author  Allen Moore
   * @param   object elem the button object.
   * @returns {void}
   */
  changeBtnColor(elem) {
    if (false === this.onAir) {
      elem.classList.add('-onair');
    }
  },

  /**
   * Method to reset the button style properties.
   *
   * @author  Allen Moore
   * @param   object elem the button object.
   * @returns {void}
   */
  resetBtnColor(elem) {
    if (true === this.onAir) {
      elem.classList.remove('-onair');
    }
  },

  /**
   * Method to get all buttons.
   *
   * @author  Allen Moore
   * @returns {void}
   */
  getBtns() {
    const btns = document.querySelectorAll("a[href='/watch/']");

    return btns;
  },

  /**
   * Method to return the start time.
   *
   * @author  Allen Moore
   * @returns {string} the start time formatted as a string.
   */
  getStartTime() {
    const date = new Date(`${this.date} ${this.startTime}`),
      dateStr = date.toString();

    return dateStr;
  },


  /**
   * Method to return the end time.
   *
   * @author  Allen Moore
   * @returns {string} the end time formatted as a string.
   */
  getEndTime() {
    const date = new Date(`${this.date} ${this.endTime}`),
      dateStr = date.toString();

    return dateStr;
  },

  /**
   * Method to return end time buffer.
   *
   * @author  Allen Moore
   * @returns {string} the end time buffer formatted as a string.
   */
  getEndBuffer() {
    const date = new Date(`${this.date} ${this.endBuffer}`),
      dateStr = date.toString();

    return dateStr;
  },

  /**
   * Method to return the current time.
   *
   * @author  Allen Moore
   * @returns {string} the current time formatted as a string.
   */
  getCurrentTime() {
    const date = new Date(),
      dateStr = `${date.toDateString()} ${date.toTimeString()}`;

    return dateStr;
  },

  /**
   * Method to compare the current time with the date of the upcoming Sunday.
   *
   * @author  Allen Moore
   * @returns {boolean} returns true/false based on the start, end, and current times.
   */
  compareTimes() {
    const curTime = this.getCurrentTime(),
      endTime = this.getEndTime(),
      startTime = this.getStartTime();

    return Boolean(curTime >= startTime && curTime <= endTime);
  },

  /**
   * Method to compare times against the end time buffer.
   *
   * @author  Allen Moore
   * @returns {boolean} returns true/false based on the start, end, end buffer, and current times.
   */
  checkEndBuffer() {
    const curTime = this.getCurrentTime(),
      endTime = this.getEndTime(),
      endBuffer = this.getEndBuffer(),
      startTime = this.getStartTime();

    return Boolean(curTime <= endTime && curTime >= endBuffer);
  },

  /**
   * Method to change all buttons.
   *
   * @author  Allen Moore
   * @returns {void}
   */
  changeAllBtns() {
    const btns = this.getBtns();
    let btn;

    for (let i = 0, len = btns.length; i < len; i++) {
      btn = btns[i];
      this.changeBtnColor(btn);
      this.changeBtnText(btn);
    }
  },

  /**
   * Method to reset all buttons.
   *
   * @author  Allen Moore
   * @returns {void}
   */
  resetAllBtns() {
    const btns = this.getBtns();
    let btn;

    for (let i = 0, len = btns.length; i < len; i++) {
      btn = btns[i];
      this.resetBtnColor(btn);
      this.resetBtnText(btn);
    }
  },

  /**
   * Method to handle the on air state changes.
   *
   * @author Allen Moore
   * @param  object int the time interval object.
   * @return {void}
   */
  handleOnAirState(int) {
    this.changeAllBtns();
    clearInterval(int);
    this.onAir = true;
  },

  /**
   * Method to handle the off air state changes.
   *
   * @author Allen Moore
   * @param  object int the time interval object.
   * @return {void}
   */
  handleOffAirState(int) {
    this.resetAllBtns();
    this.onAir = false;
  },

  /**
   * Method to handle the CTA button updates.
   *
   * @author  Allen Moore
   * @returns {void}
   */
  handleUpdate() {
    let timeInt = null,
      updateBtn;

    updateBtn = () => {
      if (true === this.compareTimes() && false === this.onAir) {
        this.handleOnAirState(timeInt)
      } if (false === this.compareTimes() && true === this.onAir) {
        this.handleOffAirState();
      }
    };
    updateBtn();
    timeInt = setInterval(updateBtn, this.delay);
  },

  /**
   * Method to initialize the watch live button.
   *
   * @author  Allen Moore
   * @returns {void}
   */
  init() {
    this.handleUpdate();
  }
};

watchLiveBtn.init();
